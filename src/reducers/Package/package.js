/**
 * @providesModule ReducersPackagePackage
 */
import UtilAsyncStorage from 'UtilAsyncStorage';
const defaultState = {
    package_id: '',
    auth: false,
    discription: 'introduction',
    comment: '',
    classify: 'home',
    tabSelect: 'camera',
    landmarkId: '',
    typeIntegration: null,
    title: '',
    keySearch: '',
    locationIntegration: '',
    region: {
        latitude: 16.0755836,
        longitude: 108.2220439,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02,
    },
    titleMarker: '',
    arrayPackageIntegration: [],
    typeIntegration: '',
    file: '',
    name: '',
    introduction: '',
    address: '',
    phone: '',
    idIntegration: '',
    errorname: '',
    erroraddress: '',
    errorphone: '',
    errorintroduce: '',
    errorlat_long: '',
    errorpackage: '',
    errortitle: '',
    errorimage: ''
};

export default function ReducersPackagePackage(state = defaultState, action) {
    switch (action.type) {
        case 'PACKAGE_AUTH':
            return Object.assign({}, state, {
                auth: action.auth,

            });
        case 'ADD_TO_CART':
        UtilAsyncStorage.AddtoCart(action.product);
            return Object.assign({}, state, {
                product: action.product,

            });
        case 'TAB_DETAIL':
            return Object.assign({}, state, {
                discription: action.discription,

            });
        case 'COMMENT':
            return Object.assign({}, state, {
                comment: action.comment,

            });
        case 'CLASSIFY':
            return Object.assign({}, state, {
                classify: action.classify,
            });
        case 'LANDMARK':
            return Object.assign({}, state, {
                tabSelect: action.tabSelect,
                landmarkId: action.landmarkId

            });
        case 'SELECTEDTAB':
            return Object.assign({}, state, {
                tabSelect: action.tab,

            });
        case 'TYPEINTEGRATION':
            return Object.assign({}, state, {

                typeIntegration: action.typeIntegration,
                title: action.title,
                idIntegration: action.idIntegration

            });
        case 'SEARCHPACKAGE':
            return Object.assign({}, state, {

                keySearch: action.keySearch

            });
        case 'LOCATIONINTEGRATION':
            return Object.assign({}, state, {

                region: action.region,
                locationIntegration: action.locationIntegration,
                titleMarker: action.titleMarker,
            });
        case 'INTEGRATIONCHOOSEPACKAGE':
            return Object.assign({}, state, {
                arrayPackageIntegration: action.arrayPackageIntegration,

            });
        case 'SAVETEMPT':
            return Object.assign({}, state, {
                file: action.file,
                name: action.name,
                introduction: action.introduction,
                address: action.address,
                phone: action.phone

            });
        case 'SHOWERROR':
            return Object.assign({}, state, {
                erroraddress: action.erroraddress,
                errorphone: action.errorphone,
                errorintroduce: action.errorintroduce,
                errorlat_long: action.errorlat_long,
                errorpackage: action.errorpackage,
                errortitle: action.errortitle,
                errorimage:''

            });
        case 'SHOWERRORIMAGE':
            return Object.assign({}, state, {
                errorimage: action.errorimage,

            });

        default:
            return state;
    }
}