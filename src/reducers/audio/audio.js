/**
 * @providesModule ReducersAudioAudio
 */

const defaultState = {
    currentPosition: '0',
    sliding: false,
    url: '',
    song: ''
};

export default function ReducersAudioAudio(state = defaultState, action) {
    switch (action.type) {
        case 'CURRENT_POSITION':
            return Object.assign({}, state, {
                currentPosition: action.currentPosition,

            });
        case 'SLIDING':
            return Object.assign({}, state, {
                sliding: action.sliding,

            });
        case 'AUDIO':
            return Object.assign({}, state, {
                url: action.url,

            });
        case 'PLAYSONG':
            return Object.assign({}, state, {
                song: action.song,

            });


        default:
            return state;
    }

}