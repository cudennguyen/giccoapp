/**
 * @providesModule ReducersAuthIndex
 */

import { combineReducers } from "redux"
import auth from 'ReducersAuthAuth'
import ForgetPassword from 'ReducersAuthForgetPassword'
import Package from 'ReducersPackagePackage'
import Audio from 'ReducersAudioAudio'

const rootReducer = combineReducers({
    auth,
    ForgetPassword,
    Package,
    Audio
});

export default ReducersAuthIndex = rootReducer;