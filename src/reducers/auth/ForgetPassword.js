/**
 * @providesModule ReducersAuthForgetPassword
 */

const defaultState = {
    email: '',
    erroremail: ''
};

export default function ReducersAuthForgetPassword(state = defaultState, action) {
    switch (action.type) {
        case 'FORGET_PASSWORD':
            return Object.assign({}, state, {
                email: action.email,
                erroremail: state.erroremail
            });
        case 'ERROR_FORGET_PASSWORD':
            return Object.assign({}, state, {
                erroremail: action.erroremail,
                email: state.email
            });
        default:
            return state;
    }
}