/**
 * @providesModule ReducersAuthAuth
 */

import Configs from 'ConfigsIndex';
import UtilAsyncStorage from 'UtilAsyncStorage';

const defaultState = {
    isLogin: false,
    address: '',
    avatar_img: '',
    email: '',
    full_name: '',
    phone: '',
    provider_login: '',
    token: ''
};

export default function ReducersAuthAuth(state = defaultState, action) {
    switch (action.type) {
        case 'LOGIN':
            UtilAsyncStorage.setStorageUser(action.user);
            return Object.assign({}, state, {
                isLogin: true,
                address: action.user.address,
                avatar_img: action.user.avatar_img,
                email: action.user.email,
                full_name: action.user.full_name,
                phone: action.user.phone,
                provider_login: action.user.provider_login,
                token: action.user.token
            });
        case 'LOGOUT':
            return defaultState;
        case 'CHANGE_AVATAR': 
            return {
                ...state,
                avatar_img: action.avatar_img
            }
        case 'CHANGE_INFOR_USER': 
            return {
                ...state,
                full_name: action.user.full_name,
                phone: action.user.phone,
                address: action.user.address
            }
        case 'ASYNC_STORAGE': 
            return Object.assign({}, state, {
                isLogin: true,
                address: action.user.address,
                avatar_img: action.user.avatar_img,
                email: action.user.email,
                full_name: action.user.full_name,
                phone: action.user.phone,
                provider_login: action.user.provider_login,
                token: action.user.token
            });
        default:
            return state;
    }
}