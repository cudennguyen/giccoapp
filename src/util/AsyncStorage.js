/**
 * @providesModule UtilAsyncStorage
 */
import { AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex'
import UtilShowMessage from "UtilShowMessage";
import { connect } from 'react-redux'
import ActionsAuthAuth from 'ActionsAuthAuth'
import store from 'SrcIndex'
const UtilAsyncStorage = {
    getStorageUser: () => {
        return AsyncStorage.getItem('isLogin').then((data) => {
            const dataParse = JSON.parse(data);
            return dataParse;
        });
    },
    setStorageUser: (user) => {
        AsyncStorage.setItem('isLogin', JSON.stringify(user))
    },
    updateStorageUser: (info) => {
        AsyncStorage.mergeItem('isLogin', JSON.stringify(info))
    },
    updateStorageAvatar: (avatar) => {
        AsyncStorage.mergeItem('isLogin', JSON.stringify({ avatar_img: avatar }))
    },
    setLogout: () => {
        AsyncStorage.removeItem('isLogin', (err) => {
            if (err) {
                UtilShowMessage.ToastShow(err, 'danger')
            }

        })
    },
    fetchAPI: (url, body) => {
        return fetch(url, body).then((response) => {
            if (response.status == '403') {
                store.dispatch(ActionsAuthAuth.logout());
                throw new Error("Login Please!")
            }
            else
                return response.json()
        })
            .then((responseJson) => {
                return responseJson
            })
            .catch((error) => {
                console.log("!122",error)
                UtilShowMessage.ToastShow(error, 'danger')
            });
    },
    AddtoCart:(product)=>{       
        fetch(Configs.hostname +"/api/mobile/cart/addtocart",{
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            body: JSON.stringify(
                {
                    productId: product.id,
                    variationName:product.name,
                    quantity: product.quality
                })
        }).then((response) => {
            if (response.status == '403') {
                store.dispatch(ActionsAuthAuth.logout());
                throw new Error("Login Please!")
            }
            else
                return response.json()
        })
            .then((responseJson) => {
                UtilShowMessage.ToastShow("Add to cart success!")
            })
            .catch((error) => {
                UtilShowMessage.ToastShow(error, 'danger')
            });
    },
    getStorageHeader: (headerCustom = {}) => {
        return AsyncStorage.getItem('isLogin').then((data) => {
            const dataParse = JSON.parse(data);
            let header = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
            if (dataParse) {
                header['Authorization'] = dataParse.token
            }

            if (Object.keys(headerCustom).length > 0 && headerCustom.constructor === Object) {
                Object.keys(headerCustom).map((value) => {
                    header[value] = headerCustom[value];
                })
            }
            return header;
        });
    },
    getImageStream: (path, format, width, height) => {
        src = Configs.hostname + Configs.apiImage
        if (format)
            src += '&format=' + format
        if (width)
            src += '&width=' + width
        if (height)
            src += '&height=' + height

        src += '&path=' + path
        return src

    }
}
export default UtilAsyncStorage 