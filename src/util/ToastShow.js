/**
 * @providesModule UtilShowMessage
 */
import React, { Component } from 'react'
import { Toast } from 'native-base'
const UtilShowMessage = {
	ToastShow: (message, type) => {
		Toast.show({ text: message ? message.toString() : 'Error!', type: type, duration:3000 })
	}
}
export default UtilShowMessage