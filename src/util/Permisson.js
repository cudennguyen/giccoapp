/**
 * @providesModule UtilPermissionCustom
 */
import React from 'react';
import {
    Alert,
    NativeModules,
    Linking,
    PermissionsAndroid,
    Platform
} from 'react-native';
export default UtilPermissionCustom = {
    NotifyPermissionLocation: ()=> {     
        return UtilPermissionCustom.CheckPermissionLocal().then(location => {
            return location
        }).catch(err => {           
        })
    },   
    CheckPermission:async ()=> {     
        const permissions=[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,PermissionsAndroid.PERMISSIONS.CAMERA] 
        const granted = await PermissionsAndroid.requestMultiple(permissions);
        let content='';
        permissions.map((item)=>{
            if(granted['android.permission.'+item.slice(19)]!='granted')
            {        
                content=content+ item.slice(19) + ','                       
            }
        })      
         if(content!='')
            {     
                UtilPermissionCustom.NotifyPermission(content);
            }
            else{
                return true
            }   
    },
    CheckPermissionLocal: ()=> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                (position) => { resolve(position)},
                (error) => { reject(error)},
                {enableHighAccuracy: Platform.OS != 'android',timeout:1000}  
            );
        })
    },
    NotifyPermission(content) {
        Alert.alert(
            content + " Access Denied",
            "Go to Settings / Privacy /" + content + " and enable access for this app.",
            [{
                text: 'Cancel',
                style: 'cancel',
            },
            {
                text: "OK",
                onPress: () => {
                    if (Platform.OS == 'android') {
                        NativeModules.OpenSettings.openAppSettings();
                    } else {
                        Linking.openURL('app-settings:')
                    }
                }
            }]
        );
    }
}