/**
 * @providesModule UtilFile
 */
import Configs from 'ConfigsIndex';

export default UtilFile  = {
    checkFileExternal: (file) => {
        const urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        const regex = new RegExp(urlRegex);
        if (file.match(regex)) {
            return file
        } else {
            return Configs.hostname + file;
        }
        
    }
}
