/**
 * @providesModule SrcRoutes
 */

import React, { Component } from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';
import { Root } from "native-base";
import Main from 'PagesMain';
import Login from 'PagesLogin';
import Signup from 'PagesSignup';
import Profile from 'PagesProfile';
import BrandDetail from 'PagesBrandDetail';
import ForgetPassword from 'PagesForgetPassword';
import Home from 'PagesHome';
import PackageDetail from './components/Package/PackageDetail';
import PackageList from './components/Package/PackageList';
import ServicesDetail from 'ComponentsServicesDetail';
import UsePackage from 'PagesUsePackage';
import MyPackage from 'PagesMyPackage';
import ServiceList from 'ComponentsServiceList';
import CommentList from './components/FormCommentList';
import Integration from 'PagesIntegration'
import FormInForDetail from 'ComponentsFormInfo';
import FormMap from 'ComponentsFormMap';
import detailPackageMap from 'ComponentsFormMapDetail';
import FormServiceDetail from 'ComponentsServicesService';
import FormRatePackageDetail from 'ComponentFormRatePackageDetail';
import FormService from 'ComponentsFormService';
import ComponentsDetailPackageMapDetail from 'ComponentsDetailPackageMapDetail';
import ComponentsDetailPackagePlayTrial from 'ComponentsDetailPackagePlayTrial';
import PagesCart from 'PagesCart';
import Payment from 'Payment';

export default SrcRoute = class Routes extends Component {
    render() {
        return (
            <Root>
                <Router>
                    <Stack key="root" hideNavBar={true}>
                        <Scene key="login" component={Login} title="Login"   />
                        <Scene key="Payment" component={Payment} title="Payment"/>
                        <Scene key="pagesCart" component={PagesCart} title="PagesCart" />
                        <Scene key="signup" component={Signup} title="register"  />
                        <Scene key="forgetPassword" component={ForgetPassword} title="forgetPassword" />
                        <Scene key="home" component={Home} title="Home" />
                        <Scene key="main" component={Main} title="Main"  initial={true} />
                        <Scene key="profile" component={Profile} title="Profile" />
                        <Scene key="branddetail" component={BrandDetail} title="Brand Detail" />
                        {/* PackageDetail */}
                        <Scene key="packagedetail" component={PackageDetail} title="Package Detail" />
                        {/* PackageList */}
                        <Scene key="packagelist" component={PackageList} title="Package List" />
                        {/* ServiceDetail */}
                        <Scene key="servicesDetail" component={ServicesDetail} title="Services Detail" />
                        <Scene key="UsePackage" component={UsePackage} title="Use Package" />
                        <Scene key="MyPackage" component={MyPackage} title="My Package" />
                        <Scene key="serviceList" component={ServiceList} title="Service List" />
                        <Scene key="commentList" component={CommentList} title="Comment List" />
                        <Scene key="formInforPackage" component={FormInForDetail} title="Form Infor Package Detail" />
                        <Scene key="formMapPackage" component={FormMap} title="Form Map Package Detail" />
                        <Scene key="formServicePackage" component={FormServiceDetail} title="Form Service Package Detail" />
                        <Scene key="formRatePackageDetail" component={FormRatePackageDetail} title="Form Rate Package Detail" />
                        <Scene key="formService" component={FormService} title="Form service by package" />
      
                 
                    </Stack>
                </Router>
            </Root>
        )
    }
}
