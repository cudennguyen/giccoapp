/**
 * @providesModule ComponentsAdvertisement
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions
} from 'react-native'
const { width } = Dimensions.get('window');
import imgLaz from '../public/images/adv/Lazada.jpg'
export default Advertisement = class Advertisement extends Component {
    render() {
        return (
            <View style={styles.Adv}>
                <Image   resizeMode={'stretch'} style={styles.advi} source={imgLaz}></Image>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    Adv: {
        width: width,
        flex: 1,
        alignItems: 'center',
        paddingBottom:3,
        paddingTop:3
    },
    advi: {
        width :'90%',
        height :50,
     
    }
});


