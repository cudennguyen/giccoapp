/**
 * @providesModule ComponentsSlideShow
 */
import React, { Component } from 'react';
import {
    Image,
    Text,
    View,
    ScrollView,
    StyleSheet,
    Animated,
    PanResponder,
    TouchableHighlight,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import IconArrows from '../public/images/icons/next.png';
import { Icon } from 'react-native-elements';
class Slideshow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: 0,
            height: Dimensions.get('window').width * (5 / 2),
            width: this.props.width,
            scrolling: false,
        };
        this.animVal = new Animated.Value(0)
    }

    _onRef(ref) {
        this._ref = ref;
        if (ref && this.state.position !== this._getPosition()) {
            this._move(this._getPosition());
        }
    }

    _move(index) {
        const isUpdating = index !== this._getPosition();
        const x = this.state.width * index;
        this._ref.scrollTo({ x: ((this.state.width + (this.animVal._value) === (this.props.dataSource.length) * this.state.width) && index != -2) ? 0 : (this.animVal._value) + (index == -2 ? -this.state.width : this.state.width), y: 0, animated: true });
        this.setState({ position: index });
        if (isUpdating && this.props.onPositionChanged) {
            this.props.onPositionChanged(index);
        }
    }
    _getPosition() {
        if (typeof this.props.position === 'number') {
            return this.props.position;
        }
        return this.state.position;
    }

    _next() {
        const pos = this.state.position === this.props.dataSource.length - 1 ? 0 : this.state.position + 1;
        this._move(-1);
        this.setState({ position: pos });
    }

    _prev() {
        const pos = this.state.position === 0 ? this.props.dataSource.length - 1 : this.state.position - 1;
        this._move(-2);
        this.setState({ position: pos });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.position !== this.props.position) {
            this._move(this.props.position);
        }
    }

    componentWillMount() {
        const width = this.props.width || this.state.width;

        let release = (e, gestureState) => {
            const width = this.state.width;
            const relativeDistance = gestureState.dx / width;
            const vx = gestureState.vx;
            let change = 0;

            if (relativeDistance < -0.5 || (relativeDistance < 0 && vx <= 0.5)) {
                change = 1;
            } else if (relativeDistance > 0.5 || (relativeDistance > 0 && vx >= 0.5)) {
                change = -1;
            }
            const position = this._getPosition();
            if (position === 0 && change === -1) {
                change = 0;
            } else if (position + change >= this.props.dataSource.length) {
                change = (this.props.dataSource.length) - (position + change);
            }
            this._move(position + change);
            return true;
        };

        this._panResponder = PanResponder.create({
            onPanResponderRelease: release
        });

        this._interval = setInterval(() => {
            const newWidth = this.props.width;
            if (newWidth !== this.state.width) {
                this.setState({ width: newWidth });
            }
        }, 16);
    }
    render() {

        const width = this.props.width || this.state.width;
        const height = this.props.height || this.state.height;
        const position = this._getPosition();
        return (
            <View style={[this.props.containerStyle,{ height: height }]}>
                <ScrollView
                    ref={ref => this._onRef(ref)}
                    decelerationRate={0.99}
                    horizontal={true}
                    pagingEnabled={true}
                    onScroll={
                        Animated.event(
                            [{ nativeEvent: { contentOffset: { x: this.animVal } } }]
                        )
                    }
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={this.props.scrollEnabled}
                    {...this._panResponder.panHandlers}
                    style={[styles.container, { height: height }]}>
                    {this.props.dataSource.map((image, index) => {
                        const imageObject = typeof image.url === 'string' ? { uri: image.url } : image.url;
                        const textComponent = (
                            <View style={styles.layoutText}>
                                {image.title === undefined ? null : <Text style={styles.textTitle}>{image.title}</Text>}
                                {image.caption === undefined ? null : <Text style={styles.textCaption}>{image.caption}</Text>}
                            </View>
                        );
                        const imageComponent = (
                            <View key={index} style={styles.containerImage}>
                                <Image
                                    source={imageObject}
                                    resizeMode={'stretch'}
                                    style={{ width: width, height: height }} />
                                {textComponent}
                            </View>
                        );
                        const imageComponentWithOverlay = (
                            <View key={index} style={styles.containerImage}>
                                <View>
                                    <Image
                                        source={imageObject}
                                        style={{}}
                                    />

                                </View>
                                {textComponent}
                            </View>
                        );
                        if (this.props.onPress) {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    style={{ height, width, borderRadius: 10 }}
                                    onPress={() => this.props.onPress({ image, index })}
                                    delayPressIn={200}>
                                    {this.props.overlay ? imageComponentWithOverlay : imageComponent}
                                </TouchableOpacity>
                            );
                        } else {
                            return this.props.overlay ? imageComponentWithOverlay : imageComponent
                        }
                    })}
                </ScrollView>
                <View
                    style={[
                        layoutArrow(this.props.height, this.props.arrowSize),
                        { left: 10 },
                    ]}>
                    <TouchableOpacity
                        onPress={() => this._prev()}>
                        {
                            this.props.arrowRight == undefined ?
                                <View >
                                    <Image style={{ width: 20, height: 20, transform: [{ rotate: '180 deg' }] }} source={IconArrows} />
                                </View>
                                :
                                this.props.arrowLeft
                        }
                    </TouchableOpacity>
                </View>
                <View
                    style={[
                        layoutArrow(this.props.height, this.props.arrowSize),
                        { right: 10 },
                    ]}>
                    <TouchableOpacity
                        onPress={() => this._next()}>
                        {
                            this.props.arrowRight == undefined ?
                                <View>
                                    <Image style={{ width: 20, height: 20 }} source={IconArrows} />
                                </View>
                                :
                                this.props.arrowRight
                        }
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    indicator: {
        margin: 3,
        opacity: 0.9
    },
    indicatorSelected: {
        opacity: 1,
    },
    containerImage: {
        flex: 1,
    },
    layoutText: {
        position: 'absolute',
        paddingHorizontal: 15,
        bottom: 30,
        left: 0,
        right: 0,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        backgroundColor: 'transparent',
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        color: 'white',
    },
    textCaption: {
        fontWeight: '400',
        fontSize: 12,
        color: 'white',
    }
});

export default ComponentsSlideShow = (Slideshow)
Slideshow.defaultProps = {
    height: 200,
    indicatorSize: 8,
    indicatorColor: '#CCCCCC',
    indicatorSelectedColor: '#FFFFFF',
    scrollEnabled: true,
    arrowSize: 16,
}

Slideshow.propTypes = {
    dataSource: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        caption: PropTypes.string,
        url: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })).isRequired,
    indicatorSize: PropTypes.number,
    indicatorColor: PropTypes.string,
    indicatorSelectedColor: PropTypes.string,
    height: PropTypes.number,
    position: PropTypes.number,
    scrollEnabled: PropTypes.bool,
    containerStyle: PropTypes.object,
    overlay: PropTypes.bool,
    arrowSize: PropTypes.number,
    arrowLeft: PropTypes.object,
    arrowRight: PropTypes.object,
    onPress: PropTypes.func,
    onPositionChanged: PropTypes.func,
};
const layoutArrow = function (imageHeight, iconHeight) {
    return {
        position: 'absolute',
        backgroundColor: 'transparent',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        top: (imageHeight - iconHeight) / 2,
        bottom: (imageHeight - iconHeight) / 3,
    };
}
