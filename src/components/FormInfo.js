/**
 * @providesModule ComponentsFormInfo
 */
import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Header from 'ComponentsPackageHeaderItems'
import HTML from 'react-native-render-html'
import { Container, Content } from "native-base";
class FormInfo extends Component {
    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <Header title={this.props.title} />
                <Content style={{ paddingLeft: 10, paddingRight: 10 }} showsVerticalScrollIndicator={false}>
                    <HTML html={this.props.description} />
                </Content>
            </Container>
        )
    }
}
export default ComponentsFormInfo = FormInfo;
