/**
 * @providesModule ComponentsServiceList
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    ActivityIndicator,
    Text,
    View,
    FlatList,
    Dimensions,
    Image
} from 'react-native';
import Header from 'ComponentsPackageHeaderItems';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import ActionsPackagePackage from 'ActionsPackagePackage'
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from 'UtilShowMessage'
import { Card, CardItem, Body } from 'native-base'
const { width } = Dimensions.get('window');
let page = Configs.PAGE_DEFAULT;
class ServiceList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            curentTypeService: '',
        };
    }

    componentWillMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/publish/integration/' + this.props.typeService.toLowerCase() + '/package/' + this.props.packageId +
                '?limit=' + Configs.LIMIT_DEFAULT + '&page=' + page,
                {
                    method: 'GET',
                    headers: headers
                }
            ).then((responseJson) => {
                if (responseJson.success_flg == true)
                    this.setState({
                        data: responseJson.results,
                        curentTypeService: this.props.typeService
                    })
                else {
                    UtilShowMessage.ToastShow(responseJson.errors, 'danger')
                }
            })

        })
    }
    goToDetail(id) {
        Actions.servicesDetail({ serviceId: id, titleService: id });
    }

    refresh() {

        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/publish/integration/' + this.props.typeService.toLowerCase() + '/package/' + this.props.packageId +
                '?limit=' + Configs.LIMIT_DEFAULT + '&page=1', {
                    method: 'GET',
                    headers: headers
                }
            ).then((responseJson) => {
                this.setState({
                    data: responseJson.results,
                    curentTypeService: this.props.typeService
                })
            })
        })
    }
    loadMorePackage = () => {
        ++page
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/publish/integration/' + this.props.typeService.toLowerCase() + '/package/' + this.props.packageId
                + '?limit=' + Configs.LIMIT_DEFAULT + '&page=' + page,
                {
                    method: 'GET',
                    headers: headers,
                }
            ).then((responseJson) => {
                if (!responseJson.results.length > 0) {
                    return this.state
                }
                return this.setState({
                    data: [...this.state.data, ...responseJson.results],
                })
            })
        })
    }

    render() {
        if (this.state.curentTypeService != this.props.typeService) {
            this.refresh()
        }
        return (
            <View style={styles.container}>
                {this.props.tabHide == true ? null :
                    <Header title={this.props.titlePackage} />}
                <FlatList
                    ref="list"
                    refreshing={this.state.refresh}
                    numColumns={2}
                    inverted={false}
                    contentContainerStyle={{ margin: 5 }}
                    horizontal={false}
                    onEndReachedThreshold={0.2}
                    onEndReached={this.loadMorePackage}
                    showsVerticalScrollIndicator={false}
                    style={styles.content}
                    data={this.state.data}
                    renderItem={
                        ({ item }) =>
                            <View style={{
                                paddingRight: 10,
                                paddingBottom: 5
                            }}>
                                <Card>
                                    <CardItem cardBody button
                                        onPress={() => this.goToDetail(item._id)}
                                        style={{ flexDirection: 'column' }}>
                                        <Image style={styles.imageStyle} source={{ uri: UtilAsyncStorage.getImageStream(item.image, null, imageWidth, imageHeight) }} />
                                        <Text numberOfLines={1} style={styles.contentBoxText}>{item.title}</Text>
                                    </CardItem>
                                </Card>
                            </View>
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}
const imageWidth = width * 0.445;
const imageHeight = (imageWidth / 933) * 800;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.fontColor
    },
    content: {
        flex: 1,
        margin: 8,
        marginBottom: 0,
    },
    contentBox: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    items: {
        backgroundColor: Configs.fontColor,
        height: width / 3.5,
        marginBottom: 10,
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        padding: 8,
        borderRadius: 4,
    },
    packageStyle: {
        flex: 1,
        justifyContent: 'space-between',
        paddingVertical: 5,
        paddingHorizontal: 5,
    },
    contentItem: {
        flex: 1,
    },
    contentItemLeft: {
        backgroundColor: Configs.fontColor
    },
    contentItemRight: {
        flex: 1,
        backgroundColor: Configs.fontColor
    },
    contentItemHead: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#000000'
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        borderWidth: 1,
        width: imageWidth,
        borderColor: Configs.visibleColor,
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        oncatogorySearch: (classify) => {
            dispatch(ActionsPackagePackage.catogorySearch(classify));

        },
        onTabName: (discription) => {
            dispatch(ActionsPackagePackage.tabDetail(discription));

        },
    }
}

export default ComponentsServiceList = connect(mapStateToProps, mapDispatchToProps)(ServiceList)
