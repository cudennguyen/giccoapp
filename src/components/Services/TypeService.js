/**
 * @providesModule ComponentsServicesTypeService
 */

import React, { Component } from 'react';
import { View, StyleSheet, Picker, Dimensions, TouchableOpacity, Text, ActionSheetIOS, Platform } from 'react-native';
import Configs from 'ConfigsIndex';
const { height } = Dimensions.get('window');
import { connect } from 'react-redux'
import ActionsPackagePackage from 'ActionsPackagePackage'
import Icon from 'react-native-vector-icons/Ionicons';
import UtilShowMessage from 'UtilShowMessage'
import UtilAsyncStorage from 'UtilAsyncStorage'
let BUTTONSCANCEL;
class TypeService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemValue: '',
        };
    }
    componentWillMount() {
        services = []
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/category',
            {
                method: 'GET',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
            }
        ).then((responseJson) => {
            responseJson.map((item) => {
                servicesArray = []
                for (var i = 0, len = responseJson.length; i < len; i++) {
                    code = responseJson[i].code
                    name = responseJson[i].type_name
                    servicesArray.push(name)
                }
            })
            if
            (Platform.OS == 'ios') servicesArray.push("Cancel")
            this.props.onTypeIntegration(code, name, responseJson[responseJson.length - 1]._id)
            this.setState({
                service: responseJson,
                servicesArray: servicesArray
            })
        })
    }
    // ActionSheet
    popActionSheet() {
        ActionSheetIOS.showActionSheetWithOptions({
            options: this.state.servicesArray,
            cancelButtonIndex: this.state.servicesArray.length - 1,
            title: 'Choose Service',

        },
            (buttonIndex) => this.actionSheetClick(buttonIndex)
        )
    }

    actionSheetClick(buttonIndex) {
        if (buttonIndex != this.state.servicesArray.length - 1)
            this.props.onTypeIntegration(this.state.service[buttonIndex].code, this.state.service[buttonIndex].type_name, this.state.service[buttonIndex]._id)
    }

    render() {
        const { container, picker } = styles;
        return (
            Platform.OS != 'ios' ? (
                <Picker
                    selectedValue={this.props.typeIntegration}
                    style={[picker, { width: this.props.width, backgroundColor: this.props.backgroundColor }]}
                    // itemStyle={pickerItem}
                    prompt={'Choose Service'}
                    onValueChange={(itemValue, itemIndex) => this.props.onTypeIntegration(itemValue, this.state.servicesArray[itemIndex], this.state.service[itemIndex]._id)}>
                    {this.state.service ?
                        (this.state.service.map((item, index) => (
                            <Picker.Item label={item.type_name} value={item.code} key={index} />
                        ))) : (null)}

                </Picker>

            ) : (
                    <View style={styles.container}>
                        <TouchableOpacity
                            style={styles.select}
                            onPress={() => this.popActionSheet()}>
                            <Text style={styles.text}>{this.props.title}</Text>
                            <Icon name="md-arrow-dropdown" size={20} style={{ marginRight: 20 }} />
                        </TouchableOpacity>
                    </View>
                )
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Configs.visibleColor,
    },

    select: {
        flex: 3 / 4,
        flexDirection: 'row',
        backgroundColor: Configs.swapBackgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        height: 30,
        borderRadius: 5,

    },

    picker: {
        flex: 3 / 4,
        backgroundColor: Configs.swapBackgroundColor,
        marginLeft: 10,
        marginRight: 10,

    },
    text: {
        flex: 1,
        marginLeft: 20
    },

});
const mapStateToProps = (state, ownProps) => {
    return {
        typeIntegration: state.Package.typeIntegration,
        title: state.Package.title,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onTypeIntegration: (type, title, idIntegration) => {
            dispatch(ActionsPackagePackage.typeIntegration(type, title, idIntegration));

        },
    }
}

export default ComponentsServicesTypeService = connect(mapStateToProps, mapDispatchToProps)(TypeService)
