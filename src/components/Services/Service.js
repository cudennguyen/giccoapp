/**
 * @providesModule ComponentsServicesService
 */

import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux';
import UtilAsyncStorage from 'UtilAsyncStorage';
import ActionsPackagePackage from 'ActionsPackagePackage'
import { Card, CardItem, Body, Container, Content } from 'native-base'
import Header from 'ComponentsPackageHeaderItems'
const { width } = Dimensions.get('window');

class FormService extends Component {
    render() {
        return (
            <Container>
                <Header title={this.props.title} />
                <Content>
                    {this.props.service.map((item) => (
                        <Service name={item.type_name} packageId={this.props.packageId} type={item.code} authPackage='false' Package='true' />
                    ))}
                </Content>
            </Container>

        )
    }
}
class Service extends Component {
    constructor(props) {
        super(props);
        this.state = {
            package: [
            ]
        };
    }

    componentWillMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/publish/integration/package/' + this.props.packageId,
                {
                    method: 'GET',
                    headers: headers,
                }
            ).then((responseJson) => {
                responseJson.results.map((item) => (
                    item.code.toUpperCase() == this.props.type ?
                        this.setState({ package: item.results }) : null
                ))
            })
        })
    }
    seeAll(value) {
        if (this.props.authPackage == 'true') {
            this.props.onShowModal(true)
        }
        else {
            Actions.serviceList({ titlePackage: value, packageId: this.props.packageId, typeService: this.props.type });
        }
    }
    goToDetail(id) {
        Actions.servicesDetail({ serviceId: id, titleService: id });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>{this.props.name}</Text>
                    {this.state.package.length > 0 ? (
                        <View style={styles.headerSeeAll}>
                            <TouchableOpacity onPress={() => this.seeAll(this.props.name, this.props.id, this.props.authPackage)}>
                                <Text style={styles.headerSeeAllText}>See All</Text>
                            </TouchableOpacity>
                        </View>) : (null)}
                </View>
                <View style={styles.content}>
                    {this.state.package.length > 0 ? (
                        <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                            {
                                this.state.package.map((item, index) => (
                                    <View style={{
                                        paddingRight: 10,
                                        paddingBottom: 5
                                    }}>
                                        <Card>
                                            <CardItem cardBody button
                                                onPress={() => this.goToDetail(item._id)}
                                                style={{ flexDirection: 'column' }}>
                                                <Image style={styles.imageStyle} source={{ uri: UtilAsyncStorage.getImageStream(item.image, null, imageWidth, imageHeight) }} />
                                                <Text numberOfLines={1} style={styles.contentBoxText}>{item.title}</Text>
                                            </CardItem>
                                        </Card>


                                    </View>
                                ))
                            }
                        </ScrollView>) : (
                            <Text style={{ alignItems: 'center' }}> None Data
                        </Text>
                        )}
                </View>
            </View>
        );
    }
}
//933 x 680
const imageWidth = width * 0.4;
const imageHeight = (imageWidth / 933) * 680;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.fontColor
    },
    header: {
        flexDirection: 'row',
        margin: 10,
        marginBottom: 0,
        marginTop: 5,
    },
    headerTitle: {
        flex: 1 / 2,
        fontSize: 15,
        color: '#000000',
        fontWeight: 'bold',
    },
    headerSeeAll: {
        flex: 1 / 2,
        alignItems: 'flex-end',
    },
    headerSeeAllText: {
        fontSize: 14,
        color: Configs.backgroundColor,
        fontWeight: 'bold',
    },
    content: {
        flex: 1,
        marginLeft: 6,
        paddingBottom: 10,
        marginBottom: 10,
    },
    contentBox: {
        marginRight: 10,
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        fontSize: 12,
        borderColor: Configs.visibleColor,
        width: imageWidth
    }
});


const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onShowModal: (auth) => {
            dispatch(ActionsPackagePackage.packageAuth(auth));

        },
    }
}

export default ComponentsServicesService = connect(mapStateToProps, mapDispatchToProps)(FormService)
