/**
 * @providesModule ComponentsServicesDetail
 */
import React, { Component } from 'react';
import { Text, ScrollView, StyleSheet, View, TouchableOpacity, Platform } from 'react-native';
import { Icon, Header } from "native-base";
import Configs from 'ConfigsIndex';
import FormViewStarReview from 'ComponentsFormViewStarReview';
import FormInforService from 'ComponentsFormInforService';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import { connect } from 'react-redux';
import UtilAsyncStorage from 'UtilAsyncStorage';
import UtilShowMessage from 'UtilShowMessage';
import ComponentsDetailPackageMap from 'ComponentsDetailPackageMap';
import ComponentsActivityIndicator from 'ComponentsActivityIndicator';
import ComponentsDetailServiceComment from 'ComponentsDetailServiceComment';
class ComponentsServicesDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
    }

    componentDidMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/publish/integration/' + this.props.serviceId, {
                    method: 'GET',
                    headers: headers
                }
            ).then((responseJson) => {
                responseJson.success_flg == true ?
                    this.setState({ data: responseJson.results }) :
                    UtilShowMessage.ToastShow(responseJson.message, 'danger')
            })
        })
    }

    goBack() {
        Actions.pop()
    }

    render() {
        if (!this.state.data.title) {
            return (<ComponentsActivityIndicator />)
        }
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Header style={{ backgroundColor: Configs.backgroundColor, height: Platform.OS == 'ios' ? 20 : 10 }}
                    iosBarStyle='light-content'
                    androidStatusBarColor={Configs.backgroundColor} />
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>
                    <FormInforService data={this.state.data} goBack={this.goBack.bind(this)} />
                    <View style={styles.container}>
                        <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Information</Text>
                                <TouchableOpacity style={styles.buttonMore} onPress={() => Actions.formInforPackage({ description: this.state.data.introduce, title: this.state.data.title })}>
                                    <Icon name="arrow-forward" style={styles.iconMore} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <HTML html={this.state.data.introduce} />
                            </View>
                        </View>
                        {this.props.isLogin ? <ComponentsDetailServiceComment serviceId={this.props.serviceId} /> : null}
                        <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Rate</Text>
                                <TouchableOpacity style={styles.buttonMore} onPress={() => Actions.formRatePackageDetail({
                                    starRating: this.state.data.star_rating,
                                    detailStar: this.state.data.rates,
                                    totalRate: this.state.data.totalRate,
                                    title: this.state.data.title,
                                    packageId: this.props.serviceId,
                                    isService: true
                                })}>
                                    <Icon name="arrow-forward" style={styles.iconMore} />
                                </TouchableOpacity>
                            </View>
                            <FormViewStarReview starRating={this.state.data.star_rating}
                                detailStar={this.state.data.rates}
                                totalRate={this.state.data.totalRate} />
                        </View>
                        <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Map</Text>
                            </View>
                            <ComponentsDetailPackageMap landmarks={[{ lat_long: this.state.data.lat_long }]} isService={true} />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },

    cardItem: {
        borderTopWidth: 1,
        borderTopColor: Configs.visibleColor,
        marginBottom: 20,
    },

    cardHeader: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
    },
    iconMore: {
        fontSize: 18,
        color: 'black'
    },

    buttonMore: {
        flex: 1,
        alignItems: 'center',
    },
    button: {
        top: '10%',
        bottom: '10%',
        borderColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'flex-end',
        width: 40,
        height: 30,
        marginBottom: 10,
        right: 10,
    },
    title: {
        flex: 7,
        fontSize: 18,
        color: 'black'
    },
    columnForm: {
        flexDirection: 'column',
        borderTopWidth: 1,
        borderTopColor: Configs.visibleColor
    },
    rowForm: {
        flexDirection: 'row', marginTop: 10,
    }

});
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentsServicesDetail = connect(mapStateToProps, null)(ComponentsServicesDetail)
