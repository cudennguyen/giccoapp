/**
 * @providesModule ComponentsHomeInputText
 */

import React, { Component } from 'react';
import { View, TextInput, Image, StyleSheet } from 'react-native';

export default ComponentsHomeInputText = class InputText extends Component {
    render() {
        return (
            
            <View style={styles.inputWrapper}>
                <Image source={this.props.image} style={styles.inlineImg} />
                <TextInput
                   style={[styles.inputBox, { borderColor: this.props.error ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                    onBlur={this.props.onBlur}
                    source={this.props.image}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder={this.props.placeholder}
                    placeholderTextColor="#ffffff"
                    selectionColor="#fff"
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                    onSubmitEditing={this.props.onSubmitEditing}
                    ref={this.props.ref} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    inputWrapper: {
        width: '100%',
        flex: 1,
        justifyContent: 'flex-end',
        marginVertical: 4
    },
    inputBox: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        width: '90%',
        height: 40,
        paddingLeft: 45,
        borderWidth: 1,
        borderRadius: 20,
        marginHorizontal: 20,
        fontSize: 16,
        color: '#ffffff',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',

    },
    inlineImg: {
        position: 'absolute',
        zIndex: 100,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },

})
