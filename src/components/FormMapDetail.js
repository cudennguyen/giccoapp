/**
 * @providesModule ComponentsFormMapDetail
 */
import React, { Component } from 'react'
import {
    View, Text, StyleSheet, Image, WebView, ToastAndroid,
    ScrollView,
    Animated,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback

} from 'react-native'
import Configs from 'ConfigsIndex'
import UtilShowMessage from "UtilShowMessage";
import { connect } from 'react-redux'

const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;
const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
class FormMapDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            position: 1,
            starCount: 4,
            interval: null,
            audio: '',
            markers:
                [],
            region: null,

        };
    }
    componentWillMount() {
        let region
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/package/marker/' + this.props.packageId,
            {
                method: 'GET'

            }
        ).then((responseJson) => {
            if (responseJson['success_flg'] == true) {
                coordinate = responseJson.results[0].lat_long
                coordinate = coordinate.split(',');
                region = {
                    latitude: parseFloat(coordinate[0]),
                    longitude: parseFloat(coordinate[1]),
                    latitudeDelta: 0.02,
                    longitudeDelta: 0.02,

                }
                this.setState({
                    markers: responseJson.results,
                    region: region,
                    audio: '',
                })
            }
            else {
                UtilShowMessage.ToastShow(responseJson['message'], 'danger')
            }
        })
        this.index = 0;
        this.animation = new Animated.Value(0);
    }

    componentDidMount() {
        // We should detect when scrolling has stopped then animate
        // We should just debounce the event listener here
        this.animation.addListener(({ value }) => {
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= this.state.markers.length) {
                index = this.state.markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }

        });
    }


    render() {
        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
                (index - 1) * CARD_WIDTH,
                index * CARD_WIDTH,
                ((index + 1) * CARD_WIDTH),
            ];
            const scale = this.animation.interpolate({
                inputRange,
                outputRange: [1, 2.5, 1],
                extrapolate: "clamp",
            });
            const opacity = this.animation.interpolate({
                inputRange,
                outputRange: [0.35, 1, 0.35],
                extrapolate: "clamp",
            });
            return { scale, opacity };
        });

        // // let slidingStart = this.onSlidingStart(this.props.sliding)
        return (

            <View style={{ flex: 1, flexDirection: 'column' }}>
               
            </View>
        )
    }
}
const styles = StyleSheet.create({
    map: {
        // ...StyleSheet.absoluteFillObject,
        height: heightW / 7 * 5,
        flex: 9
    },
    scrollView: {
        position: "absolute",
        bottom: 10,
        left: 0,
        right: 0,
        paddingVertical: 100,
        height: -400
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    textContent: {
        flex: 1,
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
    modal: {
        // height: heightW / 4,
        // paddingVertical: 300,
        flexDirection: 'column',

        flex: 1,
        borderRadius: 10,
        alignItems: 'flex-start',
        backgroundColor: 'white'


    },
})

const mapStateToProps = (state, ownProps) => {
    return {
        currentPosition: state.Audio.currentPosition,
        sliding: state.Audio.sliding,
        token: state.auth.token,
        landmarkId: state.Package.landmarkId,

    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onCurrentPosition: (currentPosition) => {
            dispatch(ActionsAudioAudio.currentPosition(currentPosition));


        },
    }
}

export default ComponentsFormMapDetail = connect(mapStateToProps, mapDispatchToProps)(FormMapDetail)