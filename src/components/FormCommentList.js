/**
 * @providesModule ComponentsFormCommentList
 */
import React, { Component } from 'react'
import { View, TextInput, StyleSheet, TouchableOpacity, Dimensions,Text ,ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Configs from 'ConfigsIndex'
import FormViewComment from 'ComponentsFormViewComment'
import FormViewStarReview from 'ComponentsFormViewStarReview'
import FormReviewStar from 'ComponentsFormReviewStar'
const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
class FormCommentList extends Component {
    render() {
        return (
             <ScrollView>
                    <View style={styles.rate}>    
                     <View style={styles.rateAndReview}>                         
                         <View style={styles.formViewStarReview}>
                             <FormViewStarReview starRating={this.props.starRating}
                                            detailStar={this.props.detailStar} />
                         </View>                       
                         <View style={styles.ScrollViewComment}>
                           <Text style={styles.textTitleReview}> REVIEWS </Text>                         
                             <FormViewComment packageId={this.props.id}  limit ={100}  />   
                                                
                         </View>
                     </View>
                 </View >  
             </ScrollView>
        
        )
    }
}
const styles = StyleSheet.create({
    rateAndReview: {
               paddingTop: 10
    },
    container:{

    },
    rate: {
       
       
        padding: 10
    },
    textTitleReview:{ 
        textAlign:'center',
    },
    
    ScrollViewComment: {
        marginBottom:30,
        paddingLeft: 10,    
        paddingRight: 10,
       
    },
    formReviewStar: {
        height: 300 ,
    },
});
export default ComponentsFormCommentList = FormCommentList;