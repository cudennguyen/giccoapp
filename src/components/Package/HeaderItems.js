/**
 * @providesModule ComponentsPackageHeaderItems
 */

import React, { Component } from 'react';
import { Icon, Container, Header, Left, Body, Right, Center, Button, Title } from 'native-base';
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
import TabNav from 'ComponentsTabNav';

const { height } = Dimensions.get('window');

class HeaderItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            text: '',
            selectedTab: 'home'

        };
    }
    goBack() {
        Actions.pop();
    }
    closeControlPanel = () => {
        this.drawer.close();
    };
    openControlPanel = () => {
        this.drawer.open();
    };
    next() {
        switch (this.props.right) {
            case 'Next':
                Actions.AddPackage()
                break;
            case 'Ok':
                break;
            case 'Save':
                break;
            default: break;
        }
    }
    goToCart = () => {
        Actions.pagesCart();
    }
    render() {

        return (
            Platform.OS == 'ios' ?

                <Header style={{ backgroundColor: Configs.backgroundColor }}
                    iosBarStyle='light-content'
                    androidStatusBarColor={Configs.backgroundColor}
                    style={{
                        borderBottomWidth: 0,
                        shadowRadius: 0,
                        shadowOffset: { height: 0, width: 0 },
                        shadowOpacity: 0,
                        backgroundColor: Configs.backgroundColor,
                        elevation: 0
                    }}
                >
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' onPress={() => this.goBack()} style={{ color: 'white' }} />
                        </Button>
                    </Left>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Title style={styles.textTitle}>{this.props.title}</Title>
                    </View>
                    <Right>
                        <TouchableOpacity onPress={this.goToCart.bind(this)} style={{ marginTop: 13 }}>
                            <View >
                                <Icon name="cart" size={22} />
                            </View>
                        </TouchableOpacity>
                    </Right>
                </Header>
                :
                <View style={styles.header}>

                    <View style={styles.imgBack}>
                        <TouchableOpacity onPress={this.goBack.bind(this)}>
                            <Icon name="md-arrow-back" size={25} style={{ color: Configs.fontColor }} />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{ flex: 8 / 10, }}>
                        <Text style={styles.textTitle}>{this.props.title}</Text>
                    </View>
                  
                    {this.props.cart!=false?  
                    <View style={{marginRight:-30}}>
                    <TouchableOpacity onPress={this.goToCart.bind(this)} style={{marginRight:5 }}>
                            <View >
                                <Icon name="cart" size={22} />
                            </View>
                        </TouchableOpacity>
                    </View>:null
                    }
                </View>
        );
    }
}

const styles = StyleSheet.create({
    imgBack: {
        paddingLeft: 15,
        flex: 1 / 10,
    },
    header: {
        height: height / 16,
        zIndex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Configs.backgroundColor,
        borderColor: Configs.backgroundColor,
    },
    textTitle: {
        textAlign: 'center',
        color: 'white',
        fontSize: 14

    }
});

export default ComponentsPackageHeader = HeaderItems;