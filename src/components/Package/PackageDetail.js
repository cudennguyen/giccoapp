/**
 * @providesModule ComponentsPackageDetail
 */
import React, { Component } from 'react';
import { Text, ScrollView, StyleSheet, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
import Header from 'ComponentsPackageHeaderItems';
import FormInforPackage from 'ComponentsFormInforPackage';
import HTML from 'react-native-render-html';
import { Icon } from 'native-base';
import UtilAsyncStorage from 'UtilAsyncStorage';
import ComponentsActivityIndicator from 'ComponentsActivityIndicator';
import { connect } from 'react-redux';
class PackageDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            creator: '',
            status: false,
            starCount: 0,
            defaultStart: 0,
            isRated: false,
            comment: '',
            yourComment: {}
        };
    }

    componentDidMount() {      
            UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/mobile/products/'+this.props.idProduct,
                {
                    method: 'GET'
                }
            ).then((responseJson) => {
                this.setState({
                    data: responseJson,
                    id: responseJson.id
                })
            })
        
    }

    render() {
        if (!this.state.data.name) {
            return (<ComponentsActivityIndicator />)
        }
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Header title={this.state.data.name} />
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>
                    <FormInforPackage data={this.state.data} />
                    <View style={styles.container}>
                        <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Information</Text>
                                <TouchableOpacity style={styles.buttonMore} onPress={() => Actions.formInforPackage({ description: this.state.data.description, title: this.state.data.title })}>
                                    <Icon name="arrow-forward" style={styles.iconMore} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <HTML html={this.state.data.description.replace(/\/user-content\//g, Configs.hostname+"/user-content/")} />
                              
                            </View>
                        </View>
                        {/* <ComponentsDetailPackageAudioTrial landmarks={this.state.data.landmarks} /> */}
                        {/* {this.props.isLogin ? <ComponentsDetailPackageComment idPackage={this.props.idProduct} /> : null}
                        <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Rate</Text>
                                <TouchableOpacity style={styles.buttonMore} onPress={() => Actions.formRatePackageDetail({
                                    starRating: this.state.data.star_rating,
                                    detailStar: this.state.data.rates,
                                    totalRate: this.state.data.totalRate,
                                    title: this.state.data.title,
                                    packageId: this.props.idProduct
                                })}>
                                    <Icon name="arrow-forward" style={styles.iconMore} />
                                </TouchableOpacity>
                            </View>
                            <FormViewStarReview starRating={this.state.data.star_rating}
                                detailStar={this.state.data.rates}
                                totalRate={this.state.data.totalRate} />
                        </View> */}
                        {/* <View style={styles.cardItem}>
                            <View style={styles.cardHeader}>
                                <Text style={styles.title}>Map</Text>
                                <TouchableOpacity style={styles.buttonMore}
                                    onPress={() => Actions.componentsDetailPackageMapDetail({ packageId: this.props.idProduct, title: this.state.data.title, isHeader: true })}>
                                    <Icon name="arrow-forward" style={styles.iconMore} />
                                </TouchableOpacity>
                            </View>
                            <ComponentsDetailPackageMap landmarks={this.state.data.landmarks} />
                        </View> */}
                        {/* {this.state.data.integrations && this.state.data.integrations.length > 0 ?
                            <View style={styles.cardItem}>
                                <View style={styles.cardHeader}>
                                    <Text style={styles.title}>Services</Text>
                                    <TouchableOpacity style={styles.buttonMore}
                                        onPress={() => Actions.formService({ packageId: this.props.idProduct })}>
                                        <Icon name="arrow-forward" style={styles.iconMore} />
                                    </TouchableOpacity>
                                </View>
                                <ComponentsDetailPackageService integrations={this.state.data.integrations} />
                            </View> : null
                        } */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },

    cardItem: {
        borderTopWidth: 1,
        borderTopColor: Configs.visibleColor,
        marginBottom: 20,
    },

    cardHeader: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
    },
    iconMore: {
        fontSize: 18,
        color: 'black'
    },

    buttonMore: {
        flex: 1,
        alignItems: 'center',
    },
    button: {
        top: '10%',
        bottom: '10%',
        borderColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'flex-end',
        width: 40,
        height: 30,
        marginBottom: 10,
        right: 10,
    },
    title: {
        flex: 7,
        fontSize: 18,
        color: 'black'
    },
    columnForm: {
        flexDirection: 'column',
        borderTopWidth: 1,
        borderTopColor: Configs.visibleColor
    },
    rowForm: {
        flexDirection: 'row', marginTop: 10,
    }

});
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentsPackageDetail = connect(mapStateToProps, null)(PackageDetail)
