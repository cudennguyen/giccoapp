/**
 * @providesModule ComponentsPackageHeader
 */

import React, { Component } from 'react';
import {Icon} from 'native-base';
import {
    StyleSheet,
    Image,
    Dimensions,
    View,
    TouchableOpacity,
    Text,
    TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
import TabNav from 'ComponentsTabNav';

const { height } = Dimensions.get('window');

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            text: '',
            selectedTab: 'home'

        };
    }
    goBack() {
        Actions.pop();
    }
    closeControlPanel = () => {
        this.drawer.close();
    };
    openControlPanel = () => {
        this.drawer.open();
    };

    render() {
     
        return (
            
                <View style={styles.imgBack}>
                    <TouchableOpacity onPress={this.goBack.bind(this)}>
                        <Icon name="md-arrow-back" size={25} style={{ color: Configs.fontColor }} />
                    </TouchableOpacity>
                </View>
          
         
        );
    }
}

const styles = StyleSheet.create({  
    imgBack: {
        paddingTop:5,
        paddingLeft:15,
        flex: 1 / 10,
    },
    header: {
        height: height / 16,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Configs.backgroundColor,
    },
});

export default ComponentsPackageHeader = Header;
