import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Dimensions,
    Image,
} from 'react-native';
import Header from 'ComponentsPackageHeaderItems';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import ActionsPackagePackage from 'ActionsPackagePackage'
import { Card, CardItem, Body } from 'native-base'
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from 'UtilShowMessage'
const { width } = Dimensions.get('window');
let page = Configs.PAGE_DEFAULT;
class PackageList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        };
    }
    componentWillMount() {
        UtilAsyncStorage.fetchAPI(Configs.hostname + this.props.api,
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            }
        ).then((responseJson) => {
            console.log("2222222",responseJson)
            this.setState({ data: responseJson })
        })
    }
    goToDetail(id,name) {
        this.props.onTabName('introduction')
        Actions.packagedetail({ idProduct: id, titlePackage: name });
    }
    render() {
        return (
            <View style={styles.container}>
                <Header title={this.props.titlePackage} />
                <FlatList
                    ref="list"
                    numColumns={2}
                    inverted={false}
                    contentContainerStyle={{ margin: 5 }}
                    horizontal={false}
                    onEndReachedThreshold={0.2}
                    showsVerticalScrollIndicator={false}
                    style={styles.content}
                    data={this.state.data}
                    renderItem={
                        ({ item }) =>
                            <View style={styles.package}>
                                <Card>
                                    <CardItem cardBody button
                                        onPress={() => this.goToDetail(item.id,item.name)}
                                        style={{ flexDirection: 'column' }}>
                                        <Image style={styles.imageStyle} source={{ uri: Configs.hostname+item.thumbnailImageUrl }} />
                                        <Text numberOfLines={1} style={styles.contentBoxText}>{item.name}</Text>
                                    </CardItem>
                                </Card>
                            </View>
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}
const imageWidth = width * 0.445;
const imageHeight = (imageWidth / 933) * 800;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    content: {
        flex: 1,
        marginLeft: 6,
        paddingBottom: 10,
        marginBottom: 10,
    },
    package: {
        borderWidth: 1,
        borderColor: 'white',
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        marginRight: 10,
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        width: imageWidth,
        borderColor: Configs.visibleColor,
        borderTopWidth: 0,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        oncatogorySearch: (classify) => {
            dispatch(ActionsPackagePackage.catogorySearch(classify));

        },
        onTabName: (discription) => {
            dispatch(ActionsPackagePackage.tabDetail(discription));

        },
    }
}

export default ComponentsPackageList = connect(mapStateToProps, mapDispatchToProps)(PackageList)
