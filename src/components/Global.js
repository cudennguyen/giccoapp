module.exports = {
    colorDefault1: '#39B54A', // color default 1 (green)
    colorDefault2: '#FFFFFF', // color default 2 (white)
    colorDefault3: '#E3E2E0' // color default 3 (brown)
};
