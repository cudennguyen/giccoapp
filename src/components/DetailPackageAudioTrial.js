/**
 * @providesModule ComponentsDetailPackageAudioTrial
 */
import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, Text } from 'react-native';
import { Card, CardItem } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Configs from 'ConfigsIndex';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
class ComponentsDetailPackageAudioTrial extends Component {    
    constructor(props) {
        super(props);
        landmarksTrial = this.props.landmarks.filter(landmark => landmark.check_trial);
    }
    goToDetail(id, title) {
        Actions.componentsDetailPackagePlayTrial({ landmarkId: id, title: title });
    }
    render() {
        return (
            <View style={styles.cardItem}>
            {landmarksTrial.length > 0 ? 
            <View>
                <View style={styles.cardHeader}>
                    <Text style={styles.title}>Trial</Text>
                </View>
                <View style={styles.container}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                        {landmarksTrial.map((item, index) => (
                            <View style={styles.item} key = {index}>
                                <Card style={styles.card}>
                                    <CardItem cardBody button onPress={() => this.goToDetail(item._id, item.name)} style={styles.itemTrial}>
                                        <Image style={styles.imageStyle}  source={{ uri: Configs.hostname + '/packages/' + item.image_thumb }} />
                                        <Text numberOfLines={1} style={styles.contentBoxText} >
                                            <Icon name="md-play" size={12} style={{ color: 'black' }} /> {item.name}
                                        </Text>
                                    </CardItem>
                                </Card>
                            </View>
                        ))}
                    </ScrollView>
                </View>
                </View>  
             : null}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },

    cardHeader: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        borderTopColor: Configs.visibleColor,
        borderTopWidth: 1,
    },
    item: {
        paddingRight: 10,
        paddingBottom: 5
    },
    title: {
        flex: 7,
        fontSize: 18, 
        color: 'black'
    },

    itemTrial: {
        flex: 1,
        flexDirection: 'column'
    },
    card: {
        width: 150, 
        height: 150
    },
    imageStyle: {
        flex: 6,
        width: '100%',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        overflow: 'hidden'
    },
    contentBoxText: {
        flex: 1,
        color: '#000000',
        padding: 5,
        fontSize: 12,
    },
})
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentsDetailPackageAudioTrial = connect(mapStateToProps, null)(ComponentsDetailPackageAudioTrial)
