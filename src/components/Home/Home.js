/**
 * @providesModule ComponentsHomeHome
 */

import React, { Component } from 'react';
import { ScrollView, StyleSheet, RefreshControl } from 'react-native';
import { Header, } from 'native-base';
import Configs from 'ConfigsIndex';
import { connect } from "react-redux";
import Package from 'ComponentsHomePackage'
import Categories from 'ComponentsHomeCategories'
import BrandProduct from 'ComponentsHomeBrandProduct'
import MyPackageList from 'ComponentsHomeMyPackageList'
import UtilAsyncStorage from 'UtilAsyncStorage'
import Advertisement from 'ComponentsAdvertisement'
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            isLogin: false
        };
    }
    onRefresh() {
        this.setState({ refreshing: true });
        setInterval(() => {
            this.setState({ refreshing: false });
        }, 1000);
    }
    render() {
        const { container } = styles;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={container}
                refreshControl={
                    <RefreshControl
                        style={{ backgroundColor: 'transparent' }}
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            >
                <Advertisement />
                <Package name="Flash deal" id='most_buy' limit='5' refresh={this.state.refreshing} api={"/api/mobile/deals/flashdeal"} />
                <Categories name="Ngành hàng" id='near' limit='5' refresh={this.state.refreshing}  api={"/api/mobile/categories/group"}/>
                <BrandProduct name="Bộ sưu tập" id='near' limit='5' refresh={this.state.refreshing} api={"/api/mobile/collection"} /> 

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.visibleColor
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}


export default ComponentsHomeHome = connect(mapStateToProps, null)(Home)

