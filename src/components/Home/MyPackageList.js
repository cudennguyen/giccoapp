/**
 * @providesModule ComponentsHomeMyPackageList
 */

import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    Dimensions
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import Configs from 'ConfigsIndex'
import { connect } from 'react-redux'
import UtilAsyncStorage from 'UtilAsyncStorage'
import { Card, CardItem, Body } from 'native-base'
import UtilShowMessage from "UtilShowMessage";
const { width } = Dimensions.get('window')
class MyPackageList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            package: [
            ]
        };
    } 
    componentWillMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/publish/package/my_package?limit=' + this.props.limit,
                {method: 'GET',
                headers: headers}).then((responseJson) => {
                if (responseJson.success_flg == true)
                    this.setState({ package: responseJson.results })
                else
                    UtilShowMessage.ToastShow('error data respond', 'danger')
            })
        })
    }
    seeAll() {
        Actions.MyPackage({ titlePackage: 'My Package' })
    }
    goToDetail(id) {
        Actions.packagedetail({ idPackage: id, titlePackage: id });
    }
    render() {

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>{this.props.name}</Text>
                    <View style={styles.headerSeeAll}>
                        <TouchableOpacity onPress={() => this.seeAll()}>
                            <Text style={styles.headerSeeAllText}>See All</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.content}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                        {
                            this.state.package.map((item, index) => (
                                <View style={{
                                    paddingRight: 10,
                                    paddingBottom: 5
                                }}
                                    key={index}
                                >
                                    <Card>
                                        <CardItem cardBody button
                                            onPress={() => item.package_id ? this.goToDetail(item.package_id._id) : null}
                                            style={{ flexDirection: 'column' }}>
                                            <Image style={styles.imageStyle} source={{ uri: UtilAsyncStorage.getImageStream(item.package_id ? item.package_id.image : null, null, imageWidth, imageHeight) }} />

                                            <Text numberOfLines={1} style={styles.contentBoxText}>{item.package_id.title}</Text>
                                        </CardItem>
                                    </Card>
                                </View>
                            ))
                        }
                    </ScrollView>
                </View>
            </View>
        );
    }
}
//933 x 680
const imageWidth = width * 0.4;
const imageHeight = (imageWidth / 933) * 680;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        marginBottom: 10,
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        height: width / 2
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        marginBottom: 0,
        paddingBottom: 5,
        marginTop: 5,
    },
    headerTitle: {
        flex: 1 / 2,
        fontSize: 15,
        color: '#000000',
        fontWeight: 'bold',
    },
    headerSeeAll: {
        flex: 1 / 2,
        alignItems: 'flex-end',
    },
    headerSeeAllText: {
        fontSize: 14,
        color: Configs.backgroundColor,
        fontWeight: 'bold',
    },
    content: {
        flex: 5,
        flexDirection: 'row',
        marginLeft: 10,
        height: width / 2

    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        width: imageWidth
    }
});


const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default ComponentsHomeMyPackageList = connect(mapStateToProps, mapDispatchToProps)(MyPackageList)
