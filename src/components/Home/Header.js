/**
 * @providesModule ComponentsHomeHeader
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Dimensions,
    TextInput,
    View,
    TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';

const { height } = Dimensions.get('window');

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'home'
        };
    }
    goBack() {
        Actions.pop();
    }
    render() {
        const { header,  imgBack } = styles;
        return (
            <View style={header}>
                <View style={imgBack}>
                    <TouchableOpacity onPress={this.goBack.bind(this)}>
                        <Image style={{ width: 25, height: 25 }} source={require('../../public/images/icons/menu-30.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: height / 16,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Configs.backgroundColor,
    },

    imgBack: {
        borderRightWidth: 1,
        borderColor: Configs.visibleColor,
        paddingRight: 5
    },

});

export default ComponentsHomeHeader = Header;
