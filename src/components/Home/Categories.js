/**
 * @providesModule ComponentsHomeCategories
 */

import React, { Component } from 'react';
import {
    View, StyleSheet, Text, TouchableOpacity, ScrollView,
    Image,
    Dimensions,
    Platform
} from 'react-native';
import { Card, CardItem, Body } from 'native-base'
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux';
import ActionsPackagePackage from 'ActionsPackagePackage';
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from 'UtilShowMessage';
const { width } = Dimensions.get('window');
class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            itemValue: '',
            package: [
            ]
        };
    }
    componentWillMount() {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname +this.props.api,
            {
                method: 'GET',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

            }).then((responseJson) => {
                this.setState({ package: responseJson })

            
         })      
    }
    seeAll(value, id) {
        // if (this.props.authPackage == 'true') {
        //     this.props.onShowModal(true)
        // }
        // else {
            Actions.packagelist({ titlePackage: value, idValues: id });
        // }
    }
    goToDetail(value,id) {
        Actions.packagelist({ titlePackage: value, idValues: id, api:"/api/mobile/products/cate/"+id});

    }
    render() {
        return (
            <View style={styles.container} elevation={5}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>{this.props.name}</Text>
                    {/* <View style={styles.headerSeeAll}>
                        <TouchableOpacity onPress={() => this.seeAll(this.props.name, this.props.id, this.props.authPackage)}>
                            <Text style={styles.headerSeeAllText}>See All</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
                <View style={styles.content}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                        {
                            this.state.package.map((item, index) => (
                                <View style={{
                                    paddingRight: 10,
                                    paddingBottom: 5
                                }}
                                    key={index}
                                >
                                    <Card style={{ width:70}}>
                                        <CardItem cardBody button
                                            onPress={() =>
                                                this.goToDetail(item.name,item.id)
                                            }
                                            style={{ flexDirection: 'column' }}
                                        >
                                            <Image style={styles.imageStyle} source={{ uri: Configs.hostname + item.thumbnailImageUrl }} />
                                            <Text numberOfLines={1} style={styles.contentBoxText} >{item.name}</Text>

                                        </CardItem>

                                    </Card>
                                </View>
                            ))
                        }
                    </ScrollView>
                </View>
            </View>
        );
    }
}
//933 x 680
const imageWidth = 70;
const imageHeight = 45;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Configs.fontColor,
        marginBottom: 5,
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        height: width / 3
    },
    header: { margin: 10,
        flex: 1/2,
        flexDirection: 'row',
        marginBottom: 0,
        marginTop: 5,
    },
    headerTitle: {
        flex: 1 / 2,
        fontSize: 15,
        color: '#000000',
        fontWeight: 'bold',
    },
    headerSeeAll: {
        flex: 1 / 2,
        alignItems: 'flex-end',
    },
    headerSeeAllText: {
        fontSize: 14,
        color: Configs.backgroundColor,
        fontWeight: 'bold',
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 10,
    },

    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        overflow: 'hidden'
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        width: imageWidth
    },
    package: {
        borderWidth: 1,
        borderColor: Configs.visibleColor,
        borderColor: 'white',
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        paddingBottom: 10,
        marginRight: 10,
        marginBottom: 10,
    },
});


const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.Package.auth
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onShowModal: (auth) => {
            dispatch(ActionsPackagePackage.packageAuth(auth));

        },
    }
}

export default ComponentsHomeCategories = connect(mapStateToProps, mapDispatchToProps)(Categories)
