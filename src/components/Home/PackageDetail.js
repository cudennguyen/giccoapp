/**
 * @providesModule ComponentsHomePackageDetail
 */

import React, { Component } from 'react';
import { ScrollView, StyleSheet, RefreshControl, Text, View, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from 'ComponentsHomeHeader';
import Configs from 'ConfigsIndex';

class PackageDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }
    goBack() {
        Actions.pop();
    }
    onRefresh() {
        this.setState({ refreshing: true });
        setInterval(() => {
            this.setState({ refreshing: false });
        }, 1000);
    }
    render() {
        const { container } = styles;
        return (
            <View>
                <Header androidStatusBarColor ={Configs.backgroundColor} /> 
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={container}
                    refreshControl={
                        <RefreshControl
                            style={{ backgroundColor: 'transparent' }}
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    <Text>{this.props.idPackage}</Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.visibleColor
    }
});

export default ComponentsHomePackageDetail = PackageDetail;
