/**
 * @providesModule ComponentsDetailPackageMap
 */
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import Configs from 'ConfigsIndex';
let landmarkMaps = [];
export default class ComponentsDetailPackageMap extends Component {
    
    constructor(props) {
        super(props);
        landmarkMaps = this.props.landmarks.map(marker => {
            let lat_long = marker.lat_long.split(',');
            return { 
                latitude: parseFloat(lat_long[0]), 
                longitude: parseFloat(lat_long[1])
            };
        })
    }
    
    render() {
        return (
            <View style= {styles.container}>
             
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    }
})
