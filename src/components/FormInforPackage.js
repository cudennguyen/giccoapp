/**
 * @providesModule ComponentsFormInforPackage
 */
import React, { Component } from 'react';
import { StyleSheet, Image, Text, View, Dimensions } from 'react-native';
import Configs from 'ConfigsIndex';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import { Button } from 'native-base';
import UtilAsyncStorage from 'UtilAsyncStorage';
import UtilShowMessage from 'UtilShowMessage';
const widthW = Dimensions.get('window').width;
const heightW = Dimensions.get('window').height;
import { connect } from 'react-redux'
class ComponentsFormInforPackage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bought: Configs.BUY
        }
    }
    onAddtoCart = () => {
         this.props.onAddtoCart(this.props.data.id,this.props.data.name,1)
    }

   
    render() {
        return (
            <View style={styles.inforPackage}>
                <Image
                    source={{ uri: Configs.hostname + this.props.data.thumbnailImageUrl }}
                    style={{ width: widthW, height: heightW / 7 * 3 }} />
                <View style={styles.container}>
                    <View style={styles.content}>
                        <Text style={styles.titlePackage}>{this.props.data.name}</Text>
                        {/* <Text style={styles.creatorPackage}>{this.props.data.creator ? this.props.data.creator.full_name : ''}</Text> */}
                    </View>
                    <View style={{ paddingTop: 10 }}>
                        <Button iconLeft block onPress={() => this.onAddtoCart()}>
                            <Icon name='md-cart' style={styles.buttonTextUse} />
                            <Text style={styles.buttonTextUse}>  {this.props.data.price} {this.props.data.type_price}</Text>
                        </Button>
                    </View>
                    <View style={styles.info}>
                        <View style={styles.infoDetail}>
                            <Text style={styles.infoBold}>{this.props.data.star_rating} <Icon name="md-star" size={18} style={{ color: 'black' }} /></Text>
                            <Text style={styles.infoNormal}>30K reviews</Text>
                        </View>
                        <View style={styles.infoDetail}>
                            <Text style={styles.infoBold}>{this.props.data.bought_count} <Icon name="md-add" size={18} style={{ color: 'black' }} /></Text>
                            <Text style={styles.infoNormal}>Buy</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 10
    },

    titlePackage: {
        fontSize: 18,
        color: 'black'
    },

    creatorPackage: {
        fontSize: 12,
        color: 'black',
        marginTop: 7
    },

    info: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
    },

    infoDetail: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },

    infoBold: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black'
    },

    infoNormal: {
        fontSize: 14,
        fontStyle: 'normal',
        color: 'black'
    },

    content: {
        marginTop: 10,
        marginBottom: 10,
    },

    buttonTextUse: {
        color: 'white',
        fontSize: 18
    },

    inforPackage: {
        flex: 1
    }
});
const mapDispatchToProps = (dispatch) => {
    return {
        onAddtoCart: (id,name,quality) => {
            dispatch(ActionsPackagePackage.onAddtoCart(id,name,quality));
        }
    }
}
export default ComponentsFormInforPackage = connect( null,mapDispatchToProps)(ComponentsFormInforPackage)
