/**
 * @providesModule ComponentsChangePassword
 */

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, TextInput } from 'react-native'
import { Button } from 'native-base'
import { connect } from 'react-redux'
import Error from 'ComponentsHomeError'
import styles from 'PublicStylesStyleUser'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import UtilAsyncStorage from 'UtilAsyncStorage'
import Configs from 'ConfigsIndex'
import UtilShowMessage from 'UtilShowMessage'
class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            press: false,
            old_pass: '',
            password: '',
            confpassword: '',
            errorold_pass: '',
            errorpassword: '',
            errorconfpassword: '',

        };
        this.showPass = this.showPass.bind(this);
    }
    resetStateErrorMgs() {
        this.setState({
            errorold_pass: '',
            errorpassword: '',
            errorconfpassword: ''
        })
    }

    showPass() {
        this.state.press === false
            ? this.setState({ showPass: false, press: true })
            : this.setState({ showPass: true, press: false });
    }
    changepassword = () => {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/changepass',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(
                        {
                            old_pass: this.state.old_pass,
                            password: this.state.password,
                            confpassword: this.state.confpassword
                        })
                }
            ).then((responseJson) => {
                if (responseJson['success_flg'] == true) {
                    UtilShowMessage.ToastShow('Change password success!')
                    Actions.profile();
                }
                else {
                    this.resetStateErrorMgs()
                    responseJson['errors'].map((item) => (
                        this.setState({ ['error' + item.param]: item.msg })
                    ))
                }
            })
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputGroups}>
                    <Error value={this.state.errorold_pass} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-lock" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorold_pass ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            secureTextEntry={this.state.showPass}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Password'
                            placeholderTextColor="#ECEFF1"
                            onChangeText={(old_pass) => this.setState({ old_pass })}
                            value={this.state.old_pass}
                            ref={(input) => this.password = input} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={styles.iconEye} />
                        </TouchableOpacity>
                    </View>
                    <Error value={this.state.errorpassword} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-lock" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorpassword ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            secureTextEntry={this.state.showPass}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='New Password'
                            placeholderTextColor="#ECEFF1"
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            ref={(input) => this.confpassword = input} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={styles.iconEye} />
                        </TouchableOpacity>
                    </View>
                    <Error value={this.state.errorconfpassword} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-lock" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorconfpassword ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            secureTextEntry={this.state.showPass}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Confirm Password'
                            placeholderTextColor="#ECEFF1"
                            onChangeText={(confpassword) => this.setState({ confpassword })}
                            value={this.state.confpassword}
                            ref={(input) => this.confpassword = input} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={styles.iconEye} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flex: 3, flexDirection: 'row', marginRight: 20, margin: 20, padding: 20, paddingBottom: 20, paddingTop: 20 }}>

                    <Button style={{ paddingLeft: 20, paddingRight: 20, marginRight: 20, justifyContent: 'center' }} light onPress={this.changepassword}>
                        <Text style={styles.buttonText}>Submit</Text>
                    </Button>

                    <Button light style={{ paddingLeft: 20, paddingRight: 20, justifyContent: 'center' }} onPress={this.Cancel}>
                        <Text style={styles.buttonText}>Cancel</Text>
                    </Button>


                </View>
            </View>

        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user_id: state.auth.user_id,
        provider_login: state.auth.provider_login

    };
}

export default ComponentsChangePassword = connect(mapStateToProps)(ChangePassword)