/**
 * @providesModule ComponentsHomeImageAvartar
 */

import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
}
    from 'react-native'


    export default ComponentsHomeImageAvartar = class ImageAvatar extends Component {
        render() {
            return (
                <View>
                    <Image
                        style={styles.headerBackground}
                        source={require('../public/images/wallpaper.png')} />
                    <View style={styles.header}>
                        <View style={styles.profilepicWrap}>
                            <Image
                                style={styles.profilepic}
                                source={require('../public/images/avatar.png')} />
                        </View>
                        <Text>Name</Text>
                    </View>
                </View>
            )
        }
}
const styles = StyleSheet.create({
    headerBackground: {
        flex: 1,
        width: null,
        alignSelf: 'stretch'
    },
    header: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        backgroundColor: 'rgba(0,0,0, 0.5)'
    },
    profilepicWrap: {
        width: 180,
        height: 180,
        borderRadius: 100,
        borderColor: 'rgba(0,0,0, 0.4)',
        borderWidth: 16

    },
    profilepic: {
        flex: 1,
        width: null,
        alignSelf: 'stretch',
        borderRadius: 100,
        borderColor: '#fff',
        borderWidth: 4

    },
    name: {
        marginTop: 20,
        fontSize: 16,
        color: '#fff',
        fontWeight: 'bold'

    },
    pos: {
        fontSize: 14,
        color: '#0394c0',
        fontWeight: '300',
        fontStyle: 'italic'

    }
})