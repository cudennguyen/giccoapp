/**
 * @providesModule ComponentsDetailServiceComment
 */
import React, { Component } from 'react';
import {
    TextInput, View, StyleSheet, ScrollView,
    TouchableOpacity, Text, Platform,
    ActionSheetIOS, NativeModules, Dimensions,
    findNodeHandle
} from 'react-native';
import Configs from 'ConfigsIndex';
import { Icon } from 'native-base';
import UtilAsyncStorage from 'UtilAsyncStorage';
import UtilShowMessage from 'UtilShowMessage';
const UIManager = NativeModules.UIManager;
const widthW = Dimensions.get('window').width;

export default class ComponentsDetailServiceComment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            scrollEnabled: false,
            comment: '',
            star_number: 0,
            yourComment: {}
        }
    }

    componentDidMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            fetch(Configs.hostname + '/api/publish/integration/' + this.props.serviceId + '/my_comment', {
                method: 'GET',
                headers: headers
            }).then(data => {
                if (data.status == '403') throw new Error('Please login!');
                else return data.json();
            })
                .then(data => {
                    if (data.success_flg) {
                        this.setState({
                            isEdit: data.results.star_number ? false : true,
                            yourComment: data.results,
                            star_number: data.results ? data.results.star_number : 0,
                            comment: data.results ? data.results.comment : '',
                            scrollEnabled: data.results && data.results.star_number ? true : false,
                        })
                    } else {
                        UtilShowMessage.ToastShow(data.message, 'danger');
                    }
                })
                .catch(err => {
                    UtilShowMessage.ToastShow(err, 'danger');
                });
        })
    }

    onMenuPressed = () => {
        const labels = ['Setting', 'Cancel'];
        UIManager.showPopupMenu(
            findNodeHandle(this.menu),
            labels,
            () => { },
            (result, index) => {
                this.setState({
                    isEdit: result === 'itemSelected' && index === 0,
                    star_number: this.state.yourComment ? this.state.yourComment.star_number : 0
                })
            },
        );
    };
    onMenuPressedIOS = () => {
        ActionSheetIOS.showActionSheetWithOptions({
            options: ['Setting', 'Cancel'],
            cancelButtonIndex: 1
        }, index => {
            this.setState({
                isEdit: !index,
                star_number: this.state.yourComment ? this.state.yourComment.star_number : 0
            })
        });
    }

    sendRateStar = () => {
        if (this.state.yourComment && this.state.yourComment.star_number != this.state.star_number) {
            UtilAsyncStorage.getStorageHeader().then((headers) => {
                UtilAsyncStorage.fetchAPI(
                    Configs.hostname + '/api/publish/integration/rating', {
                        method: 'POST',
                        headers: headers,
                        body: JSON.stringify({
                            integration_id: this.props.serviceId,
                            star_number: this.state.star_number
                        })
                    }
                ).then((responseJson) => {
                    if (responseJson['success_flg'] == true) {
                        this.scrollView.scrollToEnd()
                        this.setState({
                            scrollEnabled: true,
                            yourComment: {
                                ...this.state.yourComment,
                                star_number: this.state.star_number
                            }
                        })
                    }
                    else {
                        UtilShowMessage.ToastShow(responseJson.message, 'danger')
                        this.setState({ isEdit: false })
                    }
                })
            })
        }
        else {
            this.scrollView.scrollToEnd()
        }
    }

    sendComment = () => {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            fetch(Configs.hostname + '/api/publish/integration/comment', {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    integration_id: this.props.serviceId,
                    comment: this.state.comment
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson['success_flg'] == true) {
                        this.setState({
                            isEdit: false,
                            yourComment: {
                                ...this.state.yourComment,
                                comment: this.state.comment
                            }
                        })
                    }
                    else {
                        UtilShowMessage.ToastShow(responseJson.message, 'danger')
                    }
                })
                .catch((error) => {
                    UtilShowMessage.ToastShow(error, 'danger')
                });
        })
    }

    render() {
        return (
            <View style={styles.cardItem}>
               
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    cardItem: {
        borderTopWidth: 1,
        borderTopColor: Configs.visibleColor,
        marginBottom: 20,
        flex: 1
    },
    cardHeader: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
    },
    iconMore: {
        fontSize: 18,
        color: 'black'
    },

    buttonMore: {
        flex: 1,
        alignItems: 'center',
    },

    title: {
        flex: 7,
        fontSize: 18,
        color: 'black'
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    contentComment: {
        width: widthW - 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonSend: {
        paddingTop: 10,
        fontSize: 12
    }

})
