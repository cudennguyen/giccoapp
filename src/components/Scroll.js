/**
 * @providesModule ComponentsScroll
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    RefreshControl,
    View,
    TouchableOpacity,
    Dimensions,
    TextInput,
    TouchableWithoutFeedback,
    PanResponder,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import TabNavigator from 'react-native-tab-navigator';
import Home from 'ComponentsHomeHome';
import Contact from 'ComponentsContactContact';
import Configs from 'ConfigsIndex';
import { StackedBarChart } from 'react-native-svg-charts'
import StarRating from 'react-native-star-rating';
import Hr from 'react-native-hr-plus';
import  { ScrollViewChild } from 'react-native-directed-scrollview';

const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
class Scroll extends Component {
    constructor(props) {
        super(props)
        this.state = {
            scroll: true
        }


    }
    componentWillMount() {
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
            onPanResponderGrant: (e, gestureState) => {
                this.scrollView.setNativeProps({ scrollEnabled: false })
                this.setState({
                    scroll: true
                })
            },
            onPanResponderMove: () => {

            },
            onPanResponderTerminationRequest: () => true,
            onPanResponderRelease: () => {
                this.scrollView.setNativeProps({ scrollEnabled: true })
                this.setState({
                    scroll: false
                })
            },
        })
    }
    render() {
    return (
      <ScrollView
        bounces={true}
        bouncesZoom={true}
        maximumZoomScale={2.0}
        minimumZoomScale={0.5}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
        style={styles.container}
      >
        <ScrollViewChild >
          // multi-directional scrolling content here...
        </ScrollViewChild>
        <ScrollViewChild >
          // vertically scrolling content here...
        </ScrollViewChild>
        <ScrollViewChild >
          // horizontally scrolling content here...
        </ScrollViewChild>
      </ScrollView>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    height: 1000,
    width: 1000,
  },
})
export default ComponentsScroll = Scroll