/**
 * @providesModule ComponentFormRatePackageDetail
 */
import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import FormViewStarReview from 'ComponentsFormViewStarReview'
import Configs from 'ConfigsIndex'
import FormViewComment from 'ComponentsFormViewComment';
import ComponentsDetailPackageComment from 'ComponentsDetailPackageComment';
import ComponentsDetailServiceComment from 'ComponentsDetailServiceComment';
import Header from 'ComponentsPackageHeaderItems';
import {connect} from 'react-redux';
class ComponentFormRatePackageDetail extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <View style={styles.container}>
                <Header title={this.props.title}/>
                <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
                    <View style={styles.rating}>
                        <View style={styles.rating}>
                            <Text style={styles.textTitleReview}>Rating</Text>
                        </View>
                        <View style={{flex: 3}}>
                            
                            <FormViewStarReview starRating={this.props.starRating} 
                                detailStar={this.props.detailStar} 
                                totalRate={this.props.totalRate} />
                        </View>
                    </View>
                    { this.props.isLogin ? 
                        this.props.isService ?
                            <View style={{flex: 3}}>
                                <ComponentsDetailServiceComment serviceId={this.props.packageId}/>
                            </View>
                            : 
                            <View style={{flex: 3}}>
                                <ComponentsDetailPackageComment idPackage={this.props.packageId}/>
                            </View>
                        : null }
                    <View style={{flex: 8}}>
                        <View style={styles.review}>
                            <Text style={styles.textTitleReview}>Review</Text>
                        </View>
                        <View style={{flex: 10}}>
                            <FormViewComment packageId={this.props.packageId} status={false} limit={15} isService={this.props.isService} />
                        </View>
                    </View>
                    
                </ScrollView>       
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#ffffff'
    },
    body: {
        flex: 1, 
        paddingRight: 10, 
        paddingLeft: 10
    },
    rating: {
        flex: 2, 
        paddingBottom: 10
    },
    review: {
        flex: 1, 
        borderTopColor: Configs.visibleColor, 
        borderTopWidth: 1
    },
    textTitleReview: {
        fontSize: 18,
        color: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentFormRatePackageDetail = connect(mapStateToProps, null)(ComponentFormRatePackageDetail)
