/**
 * @providesModule ComponentsSliderSound
 */
import React, { Component } from 'react';
import {
    ScrollView, StyleSheet,
    Text, View, TouchableOpacity
} from 'react-native'
import ActionsAudioAudio from 'ActionsAudioAudio';

import { connect } from 'react-redux'
import Slider from 'react-native-slider'


class SliderSound extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentPosition: 0
        }
    }
    pad(n, width, z = 0) {
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
    minutesAndSeconds = (position) => ([
        this.pad(Math.floor(position / 60), 2),
        this.pad((position % 60).toFixed(0), 2),
    ]);
    slidingComplete(value) {
        this.props.changeValue(value)
        this.props.onSlidingStart(false)
    }

    render() {
        let trackLength = this.props.trackLength
        let currentPosition = this.props.currentPosition
        let onSeek = this.props.onSeek
        let onSlidingStart = this.props.onSlidingStart
        const sed = this.minutesAndSeconds(this.props.currentPosition);
        const remaining = this.minutesAndSeconds(trackLength - this.props.currentPosition);
        return (
            <View style={styles.container}>
                <Slider
                    step={1}
                    minimumValue={0}
                    maximumValue={Math.max(trackLength, 1, this.props.currentPosition)}
                    onSlidingStart={() => this.props.onSlidingStart(true)}
                    onValueChange={(value) => this.props.onChangeValue(value)}
                    onSlidingComplete={(value) => {
                        this.slidingComplete(value)
                    }}

                    value={this.props.currentPosition}
                    style={styles.slider}
                    minimumTrackTintColor='#fff'
                    maximumTrackTintColor='rgba(255, 255, 255, 0.14)'
                    thumbStyle={styles.thumb}
                    trackStyle={styles.track}
                />
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text}>

                        {sed[0] + ":" + sed[1]}
                    </Text>
                    <View style={{ flex: 1 }} />
                    <Text style={[styles.text, { width: 60 }]}>
                        {trackLength > 1 && remaining[0] + ":" + remaining[1]}
                    </Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    slider: {
        marginTop: -12,
    },
    container: {
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 10,
    },
    track: {
        height: 2,
        borderRadius: 1,
    },
    thumb: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'white',
    },
    text: {
        marginTop: -10,
        color: 'rgba(255, 255, 255, 0.72)',
        fontSize: 12,
        textAlign: 'center',
    }
})

const mapStateToProps = (state, ownProps) => {
    return {
        currentPositionss: state.Audio.currentPosition,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onCurrentPosition: (currentPosition) => {
            dispatch(ActionsAudioAudio.currentPosition(currentPosition));

        },
        onStartSliding: (sliding) => {
            dispatch(ActionsAudioAudio.sliding(sliding));

        },
    }
}

export default ComponentsSliderSound = connect(mapStateToProps, mapDispatchToProps)(SliderSound)