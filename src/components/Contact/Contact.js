/**
 * @providesModule ComponentsContactContact
 */

import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Contact extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text>Contact component</Text>
            </View>
        );
    }
}

export default ComponentsContactContact = Contact;
