/**
 * @providesModule ComponentsFormService
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Dimensions
} from 'react-native'
import Header from 'ComponentsPackageHeaderItems'
import Configs from 'ConfigsIndex'
import TypeService from 'ComponentsServicesTypeService'
const { width } = Dimensions.get('window')
import ServiceList from 'ComponentsServiceList'
import { connect } from 'react-redux'
class FormService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            refresh: false,
            skip: 0

        };
        if (this.props.song)
            this.props.song.stop()
    }
    render() {
        return (
            <View style={styles.container}>
                <Header title={this.props.titlePackage} />
                <View style={{ flex: 1 / 10 }}>
                    <TypeService />
                </View>
                {this.props.type != '' ?
                    <ServiceList packageId={this.props.packageId} typeService={this.props.type} tabHide={true} />
                    : null}
            </View>
        );
    }
}
const imageWidth = width * 0.445;
const imageHeight = (imageWidth / 933) * 800;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.visibleColor
    },
});
const mapStateToProps = (state, ownProps) => {
    return {
        type: state.Package.typeIntegration,
        titlePackage: state.Package.title,
        song: state.Audio.song
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default ComponentsFormService = connect(mapStateToProps, mapDispatchToProps)(FormService)

