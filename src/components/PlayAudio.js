/**
 * @providesModule ComponentsPlayAudio
 */
import React, { Component } from 'react';
import {
    Dimensions,
    TouchableOpacity,
    StyleSheet,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import SliderSound from 'ComponentsSliderSound'
import Sound from 'react-native-sound'
import ActionsAudioAudio from 'ActionsAudioAudio';
import UtilShowMessage from 'UtilShowMessage'
var song = null
class PlayAudio extends Component {
    constructor(props) {
        super(props)
        this.state = {
            startaudio: false,
            paused: true,
            pauseBackup: true,
            reload: false,
            totalLength: 1,
            currentPosition: 0,
            selectedTrack: 0,
            currentUrl: '',
        }
    }
    seek(time) {
        time = Math.round(time);
        this.setState({
            currentPosition: time,
            paused: false,
        });
    }

    componentWillMount() {
        if (this.state.currentUrl != this.props.url) {
            this.pause()
        }
    }

    play() {
        this.tickInterval = setInterval(() => { this.tick(); }, 1000);
        song.play((success) => {

            if (success) {
                if (this.tickInterval) {
                    clearInterval(this.tickInterval);
                    this.tickInterval = null;
                }
                this.setState({ reload: true })
            } else {
                UtilShowMessage.ToastShow('Sorry, Sound play error!', 'danger')
            }
        })
    }

    tick() {
        song.getCurrentTime((seconds) => {
            if (this.tickInterval) {
                this.setState({
                    currentPosition: seconds,
                });
            }
        });
    }
    pause() {
        if (song)
            song.pause()
        if (this.tickInterval) {
            clearInterval(this.tickInterval);
            this.tickInterval = null;
        }
        song = new Sound(this.props.url, undefined, (error) => {
            if (error)
                UtilShowMessage.ToastShow(error, 'danger')
        })
        this.props.onPlaySong(song)
        this.setState({
            currentUrl: this.props.url,
            startaudio: false,
            paused: true,
            pauseBackup: true,
            reload: false,
            totalLength: 1,
            currentPosition: 0,
            selectedTrack: 0,
        })
    }
    onPressButtonPause() {
        if (song != null) {
            if (this.state.reload) {
                this.setState({
                    reload: false,
                    paused: false,
                    currentPosition: 0
                }
                )
                song.setCurrentTime(0)
                this.play()
            }
            else {
                if (this.state.paused)
                    this.play()
                else {
                    song.pause();
                    if (this.tickInterval) {
                        clearInterval(this.tickInterval);
                        this.tickInterval = null;
                    }
                }
                this.setState({
                    paused: !this.state.paused
                })
            }
        }
    }
    onSlidingStart(value) {
        if (value) {

            song.pause()
            if (this.tickInterval) {
                clearInterval(this.tickInterval);
                this.tickInterval = null;
            }
            this.setState({
                pauseBackup: this.state.paused,
                paused: true

            })
        }
        else {
            if (!this.state.pauseBackup) {
                this.play()
            }
            this.setState({
                reload: false,
                paused: this.state.pauseBackup
            })
        }
    }
    render() {
        if (this.state.currentUrl != this.props.url) {
            this.pause()
        }
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <SliderSound
                        onSeek={this.seek.bind(this)}
                        trackLength={song.getDuration()}
                        onSlidingStart={(val) => this.onSlidingStart(val)}
                        currentPosition={this.state.currentPosition}
                        onChangeValue={(value) => { this.setState({ currentPosition: value }) }}
                        changeValue={(val) => {
                            song.setCurrentTime(val)
                        }}
                    />
                </View>
                <View style={styles.audio}>
                    <TouchableOpacity onPress={this.onPressButtonPause.bind(this)}>
                        <Icon name={this.state.reload ? 'md-refresh' : (this.state.paused ? "md-play" : "md-pause")} size={25} style={{ color: 'white' }} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderRadius: 10,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    content: {
        flex: 1
    },
    audio: {
        flex: 1,
        alignItems: 'center',
        marginTop: -5

    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        currentPosition: state.Audio.currentPosition,
        sliding: state.Audio.sliding,
        tabSelect: state.Package.tabSelect,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onCurrentPosition: (currentPosition) => {
            dispatch(ActionsAudioAudio.currentPosition(currentPosition));

        },
        onPlaySong: (song) => {
            dispatch(ActionsAudioAudio.playSong(song))
        }
    }
}

export default ComponentsPlayAudio = connect(mapStateToProps, mapDispatchToProps)(PlayAudio)