/**
 * @providesModule ComponentsAvatarUser
 */

import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import { Thumbnail, Left, Container } from 'native-base'
import PhotoUpload from 'react-native-photo-upload'
import { connect } from 'react-redux'
import Configs from 'ConfigsIndex'
import styles from 'PublicStylesStyleUser'
import UtilShowMessage from 'UtilShowMessage'
import UtilAsyncStorage from 'UtilAsyncStorage'
class AvatarUser extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }  
    uploadAvatar = (file) => {
        UtilAsyncStorage.getStorageHeader(
            { 'Content-Type': 'multipart/form-data', 'boundary': '6ff46e0b6b5148d984f148b6542e5a5d' })
            .then((headers) => {
                UtilAsyncStorage.FetchBlob(Configs.hostname + '/api/upload_avatar',
                headers,    
                 [  { name: 'avatar', filename: 'photo.jpg', type: 'image/jpeg', data: file },]
                ).then((responseJon) => {   
                        UtilShowMessage.ToastShow("Thay đổi hình đại diện thành công!");
                        this.props.onChangeAvatar(Configs.hostname + responseJon.avatar_img);                   
                })
            })
    }
    render() {
        return (
            <View style={styles.infoUser}>
                <View>
                    <PhotoUpload
                       width={200}
                       height={200}
                       onPhotoSelect={avatar => {
                           if (avatar) {
                               this.uploadAvatar(avatar)
                           }
                       }}
                    >

                        <Thumbnail large
                            source={this.props.avatar_img ? {
                                uri: this.props.avatar_img
                            } : require('../public/images/default_avatar.jpg')}
                        />

                    </PhotoUpload>
                </View>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold', color: 'white' }}>{this.props.full_name}</Text>
                    <Text style={{ fontSize: 16, color: 'white' }}>{this.props.email}</Text>
                </View>

            </View>

        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        avatar_img: state.auth.avatar_img,
        full_name: state.auth.full_name,
        email: state.auth.email
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onChangeAvatar: (avatar_img) => {
            dispatch(ActionsAuthAuth.changeAvatar(avatar_img));
        },
    }
}

export default ComponentsAvatarUser = connect(mapStateToProps, mapDispatchToProps)(AvatarUser)