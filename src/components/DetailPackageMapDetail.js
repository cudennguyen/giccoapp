/**
 * @providesModule ComponentsDetailPackageMapDetail
 */
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Configs from 'ConfigsIndex';
import Header from 'ComponentsPackageHeaderItems';
import UtilAsyncStorage  from 'UtilAsyncStorage'
import ComponentsActivityIndicator from 'ComponentsActivityIndicator';
export default class ComponentsDetailPackageMapDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            landmarkMaps: []
        };
    }
    componentDidMount() {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/publish/package/marker/' + this.props.packageId + '?approve=1',
            { method: 'GET' }
        ).then((responseJson) => {
            if (responseJson['success_flg'] == true) {
                this.setState({ landmarkMaps: responseJson.results })
            }
            else {
                alert('Comment error!')
            }
        })        
    }

    render() {
        if (this.state.landmarkMaps.length <= 0) {
            return (<ComponentsActivityIndicator />)
        }

        const initialRegion = this.state.landmarkMaps.length > 0 ? {
            latitude: parseFloat(this.state.landmarkMaps[0].latitude),
            longitude: parseFloat(this.state.landmarkMaps[0].longitude),
            latitudeDelta: Configs.ZOOM_LATITUDE,
            longitudeDelta: Configs.ZOOM_LONGITUDE,
        } : null
        return (
            <View style={styles.container}>
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    cardHeader: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardBody: {
        flex: 9,
        flexDirection: 'column'
    },
    cardInfo: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black'
    },
    contentText: {
        flex: 1,
        fontSize: 14,
        textAlign: 'left',
        color: 'black'
    },
    contentTime: {
        fontSize: 12,
        fontStyle: 'italic',
        color: 'black'
    }

});
