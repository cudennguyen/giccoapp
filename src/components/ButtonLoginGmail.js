/**
 * @providesModule ComponentsButtonLoginGmail
 */

import React, { Component } from 'react';
import Dimensions from 'Dimensions';
import { TouchableOpacity, Text, Image, View, StyleSheet, AsyncStorage } from 'react-native';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { Actions } from 'react-native-router-flux';
import ActionsAuthAuth from 'ActionsAuthAuth';
import { connect } from 'react-redux';
import Configs from 'ConfigsIndex'
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from 'UtilShowMessage'
class ButtonLoginGmail extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
            // play services are available. can now configure library
        })
            .catch((err) => {
                // UtilShowMessage.ToastShow("Play services error" + err.code + err.message, 'danger');
            })
        GoogleSignin.configure({
            scopes: ["https://www.googleapis.com/auth/drive.readonly"],
            webClientId: "369649693627-mddp5gdpf5oqci6p623uge3iudkveg6t.apps.googleusercontent.com",
            iosClientId: "813940118896-mrli7sukait55rns5k2h0nu7jvbmavtv.apps.googleusercontent.com"
        })
            .then(() => {
                // you can now call currentUserAsync()
            });
    }
    handler() {
        GoogleSignin.signIn()
            .then((user) => {
                this.login(user);
            })
            .catch((err) => {
                UtilShowMessage.ToastShow('WRONG SIGNIN:' + err, 'danger');
            })
            .done();
    }
    login(user) {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/login_api',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        api_type: "google",
                        api_id: user['id'],
                        api_token: user['accessToken'],
                        email: user['email'],
                        full_name: user['name'],
                        avatar_img: user['photo']
                    })
            }
        ).then((responseJson) => {
            if (responseJson['success_flg'] == true) {
                const user = {
                    isLogin: true,
                    user_id: responseJson['data']['user_id'],
                    provider_login: responseJson['data']['provider_login'],
                    full_name: responseJson['data']['full_name'],
                    email: responseJson['data']['email'] ? responseJson['data']['email'] : '',
                    phone: responseJson['data']['phone'] ? responseJson['data']['phone'] : '',
                    address: responseJson['data']['address'] ? responseJson['data']['address'] : '',
                    avatar_img: responseJson['data']['avatar_img'] ? responseJson['data']['avatar_img'] : '',
                    token: responseJson['data']['token']
                }
                this.props.onLogin(user);
                Actions.main();
            }
            else {
                UtilShowMessage.ToastShow(responseJson['errors'][0]['param'] + ': ' + responseJson['errors'][0]['msg'], 'danger')
            }
        })       
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonSignGmail}
                    activeOpacity={0.5}
                    onPress={this.handler.bind(this)}
                >
                    <Image
                        source={require('../public/images/icons/Google_Plus.png')}
                        style={styles.ImageIconStyle}
                    />

                    <Text style={styles.textButtonStyle}> Google </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',

    },
    buttonSignGmail: {
        flexDirection: 'row',
        width: '80%',
        alignItems: 'center',
        backgroundColor: '#dc4e41',
        borderWidth: .5,
        borderColor: '#dc4e41',
        height: 40,
        margin: 5,
        borderWidth: 1,
        borderRadius: 5,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonStyle: {
        color: "#fff",
        marginBottom: 4,
        marginRight: 20,


    },
    ImageIconStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',

    },
});
const mapStateToProps = (state, ownProps) => {
    return {
        username: state.auth.username
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (user) => {
            dispatch(ActionsAuthAuth.login(user));

        },
    }
}

export default ComponentsButtonLoginGmail = connect(mapStateToProps, mapDispatchToProps)(ButtonLoginGmail)