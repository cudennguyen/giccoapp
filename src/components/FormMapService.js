/**
 * @providesModule ComponentsFormMapService
 */
import React, { Component } from 'react'
import {
    View, Text, StyleSheet, Image, WebView, ToastAndroid,
    ScrollView,
    Animated,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback

} from 'react-native'
import MapView from 'react-native-maps'
import PlayAudio from 'ComponentsPlayAudio'
import Slideshow from 'react-native-slideshow'
import Modal from "react-native-modal"
import Icon from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating'
import Configs from 'ConfigsIndex'


const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;
const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
class FormMapService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: [],
            region: null,
            coordinate: []

        };
    }
    componentWillMount() {
        let region = null
        if (this.props.latLong) {
            coordinates = this.props.latLong.split(',')
            region = {
                latitude: parseFloat(coordinates[0]),
                longitude: parseFloat(coordinates[1]),
                latitudeDelta: 0.02,
                longitudeDelta: 0.02,
            }
            coordinate = {
                latitude: parseFloat(coordinates[0]),
                longitude: parseFloat(coordinates[1]),
            }
        }
        this.setState({
            markers: [],
            region: region,
            audio: '',
            coordinate: coordinate
        })

        this.index = 0;
        this.animation = new Animated.Value(0);
    }

    componentDidMount() {
        // We should detect when scrolling has stopped then animate
        // We should just debounce the event listener here
        this.animation.addListener(({ value }) => {
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= this.state.markers.length) {
                index = this.state.markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }
        });
    }


    render() {
        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
                (index - 1) * CARD_WIDTH,
                index * CARD_WIDTH,
                ((index + 1) * CARD_WIDTH),
            ];
            const scale = this.animation.interpolate({
                inputRange,
                outputRange: [1, 2.5, 1],
                extrapolate: "clamp",
            });
            const opacity = this.animation.interpolate({
                inputRange,
                outputRange: [0.35, 1, 0.35],
                extrapolate: "clamp",
            });
            return { scale, opacity };
        });

        // // let slidingStart = this.onSlidingStart(this.props.sliding)
        return (

            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <MapView style={styles.map}
                        ref={map => this.map = map}
                        initialRegion={this.state.region}
                    >
                        <MapView.Marker
                            coordinate={this.state.coordinate}
                        // title={marker.name}
                        >
                            <MapView.Callout
                            // ref={marker}
                            >
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    {/* <StarRating
                                        isVisible={true}
                                        style={{ width: 200, }}
                                        disabled={true}
                                        maxStars={5}
                                        rating={marker.star_rating}
                                        starSize={10}
                                        interitemSpacing={10}
                                    />
                                    <Text>{marker.name}</Text> */}

                                </View>

                            </MapView.Callout>
                        </MapView.Marker>
                    </MapView>

                </View >
            </View>
        )
    }
}
const styles = StyleSheet.create({
    map: {
        // ...StyleSheet.absoluteFillObject,
        height: heightW / 7 * 5,
        flex: 9
    },

    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },

})



export default ComponentsFormMapService = FormMapService