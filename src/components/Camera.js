/**
 * @providesModule ComponentsUseCamera
 */

import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight,
    TouchableOpacity,
    Image,
    Text,
    FlatList,
    ImageBackground,
    ActivityIndicator,
    Alert,
    NativeModules,
    Linking,
    PermissionsAndroid,
    Platform,
    Navigator
} from 'react-native';
// import Camera from 'react-native-camera';
import Configs from 'ConfigsIndex';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux';
import ActionsPackagePackage from 'ActionsPackagePackage';
import UtilShowMessage from 'UtilShowMessage';
import PermissionCustom from 'UtilPermissionCustom';
const { width } = Dimensions.get('window');
const imageWidth = width * 0.445;
const imageHeight = (imageWidth / 933) * 800;

class UseCamera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            path: null,
            flagRespond: false,
            dataRespond: '',
            locationEnabled: false,
            cameraPermissons: false,
            lat_long: ''
        };
        Keyboard.dismiss();
        if (this.props.song)
            this.props.song.stop()
    }
    componentWillMount() { 
            PermissionCustom.CheckPermission()
            PermissionCustom.NotifyPermissionLocation().then(location => {            
            this.setState({
                lat_long: location.coords.latitude.toString().concat(',', location.coords.longitude.toString())
            })         
      });     
    }
    takePicture() {
        this.camera.capture()
            .then((data) => {
                this.setState({
                    file: data,
                    path: data.path,
                    flagRespond: false, 
                    dataRespond: ''
                })
            })
            .catch(err => UtilShowMessage.ToastShow(err, 'danger'));
    }

    goToLandmark(id) {
        this.props.onlandmark('map', id)
    }

    renderCamera() {
        return (
         <View></View>
        );
    }

    renderImage() {
        return (
            <View>
               
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>

                {this.state.path ? this.renderImage() : this.renderCamera()}
            </View>

        );
    }
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
    },
    preview: {
        flex: 1,
        paddingTop: 10,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        margin: 8,
    },
    capture: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 5,
        borderColor: '#FFF',
        marginBottom: 15,
    },
    content: {
        flex: 1,
        marginTop: 15,
        margin: 8,
        marginBottom: 10,
        backgroundColor: '#000',
        opacity: 0.7,
        borderRadius: 7,
    },
    cancel: {
        position: 'absolute',
        left: 10,
        paddingTop: 17,
        backgroundColor: 'transparent',
    },
    buttonSendComment: {
        position: 'absolute',
        right: 20,
        bottom: 20,
        backgroundColor: Configs.backgroundColor,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        borderRadius: 60,
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#FFF',
        paddingTop: 5,
        paddingEnd: 5,
        paddingLeft: 20,
        fontSize: 12,
        // width: imageWidth,
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    contentBox: {
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
    },
    packageStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: imageHeight,
        width: imageWidth * 2,
        paddingVertical: 5,
        paddingHorizontal: 5,
        padding: 5
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        user_id: state.auth.user_id,
        token: state.auth.token,
        song: state.Audio.song
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onlandmark: (tab, landmarkId) => {
            dispatch(ActionsPackagePackage.landmark(tab, landmarkId))
        },
    }
}

export default ComponentsUseCamera = connect(mapStateToProps, mapDispatchToProps)(UseCamera)
