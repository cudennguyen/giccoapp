/**
 * @providesModule ComponentsFormTabDetail
 */
import React, { Component } from 'react';
import { StyleSheet, RefreshControl, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import StarRating from 'react-native-star-rating'
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux'
import ActionsPackagePackage from 'ActionsPackagePackage';
import Icon from 'react-native-vector-icons/Ionicons';
import {Spinner} from 'native-base';
const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
const icon_name = "md-text"
class FormTabDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            discription: 'introduction',
        }
    }

    tabName(name) {
        setTimeout(() => {<Spinner/>
            }, 1000);
        this.props.onTabName(name)

    }

    render() {

        return (
            <View style={{
                flexDirection: 'row',
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                paddingLeft: 15,
                paddingRight: 15
            }}>
                {this.props.tab.map((tab) => {
                    return (
                        <TouchableOpacity onPress={() => this.tabName(tab.name)}>
                        <View style={{
                            width: widthW / this.props.tab.length,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: this.props.discription == tab.name ? Configs.visibleColor : 'white',
                            borderRightColor: '#bfbfbf',
                            borderRightWidth: 1,
                        }}>

                  
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Icon name={tab.name == 'map' ? "md-map" : (tab.name == 'rate' ? "md-pulse" : (tab.name == 'services' ? 'md-planet' : "md-text"))}
                                        size={30} style={{ color: 'black', paddingTop: 10 }} />
                                </View>
                                <Text style={styles.textTabTitle}>{tab.name}</Text>
                          
                          
                        </View>
                        </TouchableOpacity>
                    )

                })
                }
            </View>


        )
    }
}
const styles = StyleSheet.create({
    textTabTitle: {
        fontSize: 14,
        fontStyle: 'normal',
        paddingBottom: 10,
        color: 'black'
    }

});

const mapStateToProps = (state, ownProps) => {
    return {
        discription: state.Package.discription
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onTabName: (discription) => {
            dispatch(ActionsPackagePackage.tabDetail(discription));

        },
    }
}

export default ComponentsFormTabDetail = connect(mapStateToProps, mapDispatchToProps)(FormTabDetail)