/**
 * @providesModule ComponentsDetailPackageService
 */
import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, Text } from 'react-native';
import { Card, CardItem } from 'native-base';
import Configs from 'ConfigsIndex';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
class ComponentsDetailPackageService extends Component {    
    goToDetail(id) {
        Actions.servicesDetail({ serviceId: id });
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                    {this.props.integrations.map((item, index) => (
                        <View style={styles.item} key = {index}>
                            <Card style={styles.card}>
                                <CardItem cardBody button onPress={() => this.goToDetail(item._id)} style={styles.cardItem}>
                                    <Image style={styles.imageStyle}  source={{ uri: Configs.hostname + item.image }} />
                                    <Text numberOfLines={1} style={styles.contentBoxText} >{item.title}</Text>
                                </CardItem>
                            </Card>
                        </View>
                    ))}
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingTop: 10, 
        borderTopColor: Configs.visibleColor, 
        borderBottomColor: Configs.visibleColor, 
        borderTopWidth: 1, 
        borderBottomWidth: 1
    },
    item: {
        paddingRight: 10,
        paddingBottom: 5
    },
    cardItem: {
        flex: 1,
        flexDirection: 'column'
    },
    card: {
        width: 150, 
        height: 150
    },
    imageStyle: {
        flex: 6,
        width: '100%',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        overflow: 'hidden'
    },
    contentBoxText: {
        flex: 1,
        color: '#000000',
        padding: 5,
        fontSize: 12
    },
})
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentsDetailPackageService = connect(mapStateToProps, null)(ComponentsDetailPackageService)
