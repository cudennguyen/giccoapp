/**
 * @providesModule ComponentsDetailPackagePlayTrial
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, Modal, PanResponder } from 'react-native';
import Configs from 'ConfigsIndex';
import {connect} from 'react-redux';
import UtilAsyncStorage from 'UtilAsyncStorage';
import UtilShowMessage from "UtilShowMessage";
import ComponentsActivityIndicator from 'ComponentsActivityIndicator';
const widthW = Dimensions.get('window').width;
const heightW = Dimensions.get('window').height;
class ComponentsDetailPackagePlayTrial extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            position: 0,
            starCount: 0,
            name: '',
            audio: '',
            region: null,
            imageSlide: [],
            imageSlide: ''
        };
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,         
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove.bind(this),
        })
    }
    _toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }
   
    componentDidMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.imageSlide.length ? 0 : this.state.position + 1
                });
            }, 5000)
        });
        UtilAsyncStorage.getStorageHeader().then(async (headers) => {
            Promise.all([
                UtilAsyncStorage.fetchAPI(
                    Configs.hostname + '/api/publish/landmark/' + this.props.landmarkId, {
                        method: 'GET',
                        headers: headers,
                    }
                ),
                UtilAsyncStorage.fetchAPI(
                    Configs.hostname + '/api/publish/landmark/' + this.props.landmarkId + '/rating', {
                        method: 'GET',
                        headers: headers,
                    }
                )        
            ]).then((results) => {
                const [landmark, rating] = results;
                let region = null;
                let imageSlide = [];
                if (landmark.success_flg == true) {
                    const coordinate = landmark.data.lat_long ? landmark.data.lat_long.split(',') : null
                    if(coordinate) {
                        region = {
                            latitude: parseFloat(coordinate[0]),
                            longitude: parseFloat(coordinate[1]),
                            latitudeDelta: Configs.ZOOM_LATITUDE,
                            longitudeDelta: Configs.ZOOM_LONGITUDE,
                        }
                    }
                    
                    imageSlide = landmark.data && landmark.data.images ? landmark.data.images.map(image => {
                        return {url: Configs.hostname + '/packages/' + image};
                    }) : [];
                    this.setState({
                        ...this.state,
                        name: landmark.data.name,
                        audio: Configs.hostname + '/packages/' + landmark.data.audio,
                        imageThumb: Configs.hostname + '/packages/' + landmark.data.image_thumb,
                        region: region,
                        imageSlide: imageSlide
                    })
                } else {
                    throw new Error('Error data response landmark');
                }

                if(rating && rating.success_flg) {
                    this.setState({
                        ...this.state,
                        starCount: rating.results ? rating.results.star_number : 0
                    })
                }                
            }).catch((error) => {
                UtilShowMessage.ToastShow(error, 'danger')
            });
        })
        
    }
    
    onStarRatingPress(rating) {
        this.setState({starCount: rating});
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            fetch(Configs.hostname + '/api/publish/landmark/rating',
            {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    landmark_id: this.props.landmarkId,
                    star_number: this.state.starCount
                })
            }).then((response) => {
                if (response.status == '403')
                    throw new Error("Login Please!")
                else
                    return response.json()
            })
            .then((responseJson) => {
                if (responseJson['success_flg'] == true) {
                    UtilShowMessage.ToastShow("Thanks your review of you, it is helpfull for me!")
                }
                else {
                    throw new Error('Error is happen, please review again!')
                }
            })
            .catch((error) => {
                UtilShowMessage.ToastShow(error, 'danger')
            });
        });
    }
    _onPanResponderMove(event, gestureState) {
        if (gestureState.moveY - gestureState.y0 > 300) {
            this.setState({ showModal: false, position: 0 })
        }
    }
    render() {
        if(!this.state.region) {
            return (<ComponentsActivityIndicator/>)
        }
        return (
            <View  style={styles.container}>
               
            </View>
        )
    }
}
const styles = StyleSheet.create({
      
    formStarRating: {
        marginTop: 5,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingRight: 10
    },
    formSlideShow: {
        height: heightW
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    container: {
        flex: 1, 
        flexDirection: 'column'
    },
    image_thumb: { 
        width: 300, 
        height: 200, 
        borderRadius: 10 
    },
    image_marker: { 
        width: 40, 
        height: 40 
    }

})
const mapStateToProps = (state, ownProps) => {
    return {
        isLogin: state.auth.isLogin
    };
}
export default ComponentsDetailPackagePlayTrial = connect(mapStateToProps, null)(ComponentsDetailPackagePlayTrial)
