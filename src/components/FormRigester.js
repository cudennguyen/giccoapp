/**
 * @providesModule ComponentsFormRegister
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    Image,
} from 'react-native';
import Dimensions from 'Dimensions';
import ButtonLoginFacebook from 'ComponentsButtonLoginFacebook';
import ButtonLoginGmail from 'ComponentsButtonLoginGmail';
import { Actions } from 'react-native-router-flux';
import Hr from 'react-native-hr';
import Error from 'ComponentsHomeError'
import styles from 'PublicStylesStyleUser'
import Icon from 'react-native-vector-icons/Ionicons';
import Configs from 'ConfigsIndex'
import UtilShowMessage from "UtilShowMessage";
import UtilAsyncStorage from 'UtilAsyncStorage'
export default ComponentsFormRegister = class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            press: false,
            username: '',
            email: '',
            name: '',
            password: '',
            confirmPassword: '',
            errorusername: '',
            erroremail: '',
            errorname: '',
            errorpassword: '',
            errorconfirmpassword: ''

        };
        this.showPass = this.showPass.bind(this);
    }
    resetStateErrorMgs() {
        this.setState({
            errorusername: '',
            erroremail: '',
            errorname: '',
            errorpassword: '',
            errorconfirmpassword: ''
        })
    }
    showPass() {
        this.state.press === false
            ? this.setState({ showPass: false, press: true })
            : this.setState({ showPass: true, press: false });
    }
    forgetPassword() {
        Actions.forgetPassword()
    }
    validation(text, type) {
        alph = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (type == 'email') {
            if (alph.test(text)) {
                console.warn("email is correct")
            }
            else {
                console.warn("invalid text")
            }
        }
    }
    json_function = () => {
        console.log()
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/mobile/account/register',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        email: this.state.email,
                        fullName: this.state.name,
                        password: this.state.password,
                        confirmPassword: this.state.confirmPassword
                    })
            }
        ).then((responseJson) => {
          
                UtilShowMessage.ToastShow('Tạo tài khoản thành công!');
                Actions.login();
          
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputGroups}>                   
                    <Error value={this.state.erroremail} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-mail" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.erroremail ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Email'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            onSubmitEditing={() => this.name.focus()}
                            ref={(input) => this.email = input} />
                    </View>
                    <Error value={this.state.errorname} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-person" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorname ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Full Name'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(name) => this.setState({ name })}
                            value={this.state.name}
                            onSubmitEditing={() => this.password.focus()}
                            ref={(input) => this.name = input} />
                    </View>
                    <Error value={this.state.errorpassword} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-lock" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorpassword ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            secureTextEntry={this.state.showPass}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Password'
                            placeholderTextColor="#ffffff"
                            ref={(input) => this.password = input}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            onSubmitEditing={() => this.confirmPassword.focus()} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={styles.iconEye} />
                        </TouchableOpacity>
                    </View>
                    <Error value={this.state.errorconfirmpassword} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-lock" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorconfirmpassword ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            secureTextEntry={this.state.showPass}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Confirm Password'
                            placeholderTextColor="#ffffff"
                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                            value={this.state.confirmPassword
                            }
                            ref={(input) => this.confirmPassword = input} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={styles.iconEye} />
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={styles.buttons}>
                    <View style={styles.buttonSignIn}>
                        <TouchableOpacity style={styles.button}
                            onPress={this.json_function}>
                            <Text style={styles.buttonText}>
                                Sign Up
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.formConenctwith}>
                    <View style={styles.buttonNetSocial}>
                        <ButtonLoginFacebook />
                        <ButtonLoginGmail />
                    </View>
                </View>

            </View>


        )
    }
}