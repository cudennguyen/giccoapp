/**
 * @providesModule ComponentsHomeError
 */

import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default ComponentsHomeError = class Error extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: ''
        }
    }
    render() {
        return (
            <View style={{ justifyContent: 'flex-end', flexDirection: 'row', paddingRight: '10%' }}>
                <Text style={{ color: '#ff4d4d' }}>{this.props.value}</Text>
            </View>
        )
    }
}