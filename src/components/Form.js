/**
 * @providesModule ComponentsForm
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    Image,
    AsyncStorage,
    Dimensions
} from 'react-native';
import UtilAsyncStorage from 'UtilAsyncStorage'
import ButtonLoginFacebook from 'ComponentsButtonLoginFacebook';
import ButtonLoginGmail from 'ComponentsButtonLoginGmail';
import { Actions } from 'react-native-router-flux';
import Hr from 'react-native-hr';
import Error from 'ComponentsHomeError'
import ActionsAuthAuth from 'ActionsAuthAuth';
import { connect } from 'react-redux';
import { Icon, Card, CardItem } from 'native-base';
import Configs from 'ConfigsIndex';
import UtilShowMessage from 'UtilShowMessage'
const { height } = Dimensions.get('window');
class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            press: false,
            password: '1qazZAQ!',
            username: 'Admin@gicco.com',
            user_id: '',
            provider_login: '',
            full_name: '',
            errorative: '',
            errorusername: '',
            errorpassword: '',
            erroractive: '',
        };
        this.showPass = this.showPass.bind(this);
    }

    showPass() {
        this.state.press === false
            ? this.setState({ showPass: false, press: true })
            : this.setState({ showPass: true, press: false });
    }
    forgetPassword() {
        Actions.forgetPassword()
    }
    resetStateErrorMgs() {
        this.setState({
            errorusername: '',
            errorpassword: '',
            erroractive: '',
        })
    }
    validation(text, type) {
        alph = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (type == 'email') {
            if (alph.test(text)) {
                console.warn("email is correct")
            }
            else {
                console.warn("invalid text")
            }
        }
    }
    json_function = () => {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/token/create',
            {
                method: 'POST',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        username: this.state.username,
                        password: this.state.password
                    })

            }
        ).then((responseJson) => {
            if (responseJson.token) {
                const user = {
                    isLogin: true,
                    user_id: responseJson.user.id,
                    full_name: responseJson.user.fullName,
                    email: responseJson.user.email,
                    phone: responseJson.user.phoneNumber,
                    address: responseJson.user.userAddresses,
                    token: responseJson.token
                }
                this.props.onLogin(user);
                Actions.main();
            }
            else {
                this.resetStateErrorMgs()
                responseJson['errors'].map((item) => (
                    this.setState({ ['error' + item.param]: item.msg })
                ))
            }
        })
    }

    render() {
        return (

            <View style={styles.container}>
                <Error value={this.state.erroractive} />

                <View style={styles.inputGroups}>
                    <Card style={{ width: "90%", borderRadius: 8, backgroundColor: 'white', height: height / 2 }}>
                        <Error value={this.state.errorusername} />
                        <View style={styles.inputWrapper}>
                            {/* <Icon name="md-contact" size={25} style={styles.inlineImg} /> */}
                            <Text style={{ paddingLeft: 10 }}>Tài khoản</Text>
                            <TextInput
                                style={[styles.inputBox, { borderColor: this.state.errorusername ? '#8B8378' : 'rgba(255, 255, 255, 0.3)' }]}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder='User name'
                                placeholderTextColor="gray"
                                selectionColor="black"
                                onChangeText={(username) => this.setState({ username })}
                                value={this.state.username}
                                onSubmitEditing={() => this.password.focus()}
                                ref={(input) => this.username = input} />
                            <Hr style={{ backgroundColor: 'gray' }} />
                        </View>
                        <Error value={this.state.errorpassword} />
                        <View style={styles.inputWrapper}>
                            <Text style={{ paddingLeft: 10 }}>Mật khẩu</Text>
                            {/* <Icon name="md-lock" size={25} style={styles.inlineImg} /> */}
                            <TextInput
                                style={[styles.inputBox, { borderColor: this.state.errorpassword ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                                secureTextEntry={this.state.showPass}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder='Password'
                                placeholderTextColor="gray"
                                onChangeText={(password) => this.setState({ password })}
                                value={this.state.password}
                                ref={(input) => this.password = input} />
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={styles.btnEye}
                                onPress={this.showPass}>
                                <Icon name="md-eye" size={25} style={styles.iconEye} />
                            </TouchableOpacity><Hr />
                        </View>

                    </Card>
                </View>

                <View style={styles.buttons}>

                    <TouchableOpacity style={styles.button}
                        onPress={this.json_function}>
                        <Text style={styles.buttonText}>
                            Đăng nhập
                            </Text>
                    </TouchableOpacity>

                    <View style={styles.signupTextCont}>
                        <TouchableOpacity onPress={this.forgetPassword}><Text style={styles.signupbutton}>Forget password?</Text></TouchableOpacity>
                    </View>
                    <View style={styles.formConenctwith}>
                        <Hr text='Connect with'
                            lineStyle={{
                                backgroundColor: 'white',
                                height: 1
                            }}
                            textStyle={{
                                color: "white",
                                fontSize: 14,
                                textDecorationColor: "#000"
                            }}
                        />
                        <View style={styles.buttonNetSocial}>
                            <ButtonLoginFacebook />
                            <ButtonLoginGmail />
                        </View>
                    </View>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 5,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'

    },
    inputGroups: {
        width: '100%',
        flex: 7, alignItems: 'center'
    },
    formConenctwith: {
        width: '100%',
        flex: 3 / 2,
    },
    buttonNetSocial: {
        flexDirection: 'row',
    },
    inputBox: {
        width: '90%',
        height: 40,
        borderRadius: 20,
        marginHorizontal: 20,
        fontSize: 14,
        color: 'gray',
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttons: {
        flex: 6,
        width: '100%',
        alignItems: 'center',
    },
    buttonSignIn: {
        flex: 1
    },
    button: {
        // flex: 1,
        width: '40%',
        height: 40,
        top: '10%',
        bottom: '10%',
        backgroundColor: Configs.backgroundColor,
        borderColor: '#ffff',
        borderWidth: 1,
        borderRadius: 3,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',


    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: 'white',
        textAlign: 'center'
    },
    btnEye: {
        position: 'absolute',
        right: '8%',
        top: '30%',
    },
    iconEye: {
        width: 25,
        height: 25,
        color: 'rgba(0,0,0,0.2)',
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
        color: 'black'
    },
    inputWrapper: {
        width: '100%',
        flex: 1,
        marginVertical: 4
    },
    signupbutton: {
        color: 'black',
        fontSize: 16,
        fontWeight: '500'
    },
    signupTextCont: {
        flex: 1,
        width: '100%',
        top: '10%',
        alignItems: 'center',
        paddingVertical: 16,
    },
    textWithDivider: {
        color: '#bfbfbf',
        marginVertical: 10,
        paddingHorizontal: 10
    }

})
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onLogin: (user) => {
            dispatch(ActionsAuthAuth.login(user));
        },
    }
}


export default ComponentsForm = connect(null, mapDispatchToProps)(Form)
