/**
 * @providesModule ComponentsFormViewStarReview
 */
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

let detailStarStand = [];
class FormViewStarReview extends Component {
    constructor(props) {
        super(props);
        detailStarStand = [
            {_id: 5, active: '0%', disable: '100%'},
            {_id: 4, active: '0%', disable: '100%'},
            {_id: 3, active: '0%', disable: '100%'},
            {_id: 2, active: '0%', disable: '100%'},
            {_id: 1, active: '0%', disable: '100%'}
        ]

        if(this.props.detailStar) {
            detailStarStand.map((star, index) => {
                this.props.detailStar.some(stand => { 
                    if(stand.hasOwnProperty('_id') && stand._id == star._id) {
                        let active = (stand.count * 100) / (this.props.totalRate);
                        let disable = 100 - active;
                        detailStarStand[index] = {
                            _id: stand._id,
                            active: active + '%',
                            disable: disable + '%'
                        };
                    }
                })  
            })
        }
    };

    mdStar = (star) => {
        let icon = [];
        for(let i = 0; i < star; i++){
            icon.push(<Icon name="md-star" key={i}/>)
        } 
        return icon
    };

    viewStar = () => {
        return detailStarStand.map((star, index) =>
            <View style={styles.rateColumn} key={index}>
                <View style={styles.showStar}>
                    {this.mdStar(star._id)}
                </View>
                <View style={styles.showPersent}>
                    <Text style={{backgroundColor: 'black', width: star.active, height: 3}}></Text>
                    <Text style={{backgroundColor: 'gray', width: star.disable, height: 3}}></Text>
                </View>
            </View>
        )
    } 
    render() {
        return (
            <View style={styles.container}>                                
               
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    totalRate: {
        flex: 1, 
        paddingLeft: 10
    },
    detailRate: {
        flex: 3, 
        justifyContent: 'flex-start'
    },
    rateColumn: {
        flex: 1, 
        flexDirection: 'row'
    },
    showStar: {
        flex:1, 
        flexDirection: 'row', 
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    showPersent: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 20,
    }
});

export default ComponentsFormViewStarReview = FormViewStarReview