/**
 * @providesModule ComponentsFormReviewStar
 */
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, NativeModules, ScrollView, ActionSheetIOS, Dimensions, findNodeHandle, Platform, TouchableNativeFeedback } from 'react-native';

import { Icon } from 'native-base';
import Configs from 'ConfigsIndex';
import FormInputComment from 'ComponentsInputComment'
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from "UtilShowMessage";
const UIManager = NativeModules.UIManager
const widthW = Dimensions.get('window').width
const THEME_COLOR = Configs.backgroundColor;
class FormReviewStar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: false,
            starCount: 0,
            defaultStart: 0,
            isRated: false,
            selectedItem: '0',
            isModalVisible: false

        }
    }
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }
    onMenuPressed = () => {
        const labels = ['Setting'];
        const { onPress } = this.props;
        UIManager.showPopupMenu(
            findNodeHandle(this.menu),
            labels,
            () => { },
            (result, index) => {
                if (result == 'itemSelected') {
                    this.setState({
                        isRated: !this.state.isRated
                    })
                }
            },
        );
    };
    onMenuPressedIOS = () => {
        ActionSheetIOS.showActionSheetWithOptions({
            options: ['Setting', 'Cancel'],
            destructiveButtonIndex: 1,
            cancelButtonIndex: 0,
        },
            (buttonIndex) => {
                if (buttonIndex === 1) {
                    this.setState({
                        isRated: !this.state.isRated
                    })
                }
            });
    }
    sendRateStar = () => {
        if (this.state.defaultStar != this.state.starCount) {
            UtilAsyncStorage.getStorageHeader().then((headers) => {
                UtilAsyncStorage.fetchAPI(
                    Configs.hostname + '/api/rating_pkg',
                    {
                        method: 'POST',
                        headers: headers,
                        body: JSON.stringify(
                            {
                                package_id: this.props.packageId,
                                star_number: this.state.starCount

                            })
                    }
                ).then((responseJson) => {
                    if (responseJson['success_flg'] == true) {
                        this.scrollView.scrollToEnd()
                        this.setState({
                            status: true,
                            defaultStar: this.state.starCount
                        })
                    }
                    else {
                        alert(responseJson.errors)
                        this.scrollView.scrollToEnd()
                        this.setState({
                            status: true
                        })
                    }

                })
            })
        }
        else {
            this.scrollView.scrollToEnd()
        }

    }
    componentWillMount() {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            fetch(Configs.hostname + '/api/package/' + this.props.packageId + '/my_comment',
                {
                    method: 'GET', headers: headers,

                }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson['success_flg'] == true) {
                        this.setState({
                            starCount: responseJson.results.star_number,
                            defaultStar: responseJson.results.star_number,
                            comment: responseJson.results.comment,
                            isRated: responseJson.results.star_number ? true : false
                        })
                    }
                    else {
                        UtilShowMessage.ToastShow('Comment Error!', 'danger');
                    }
                })
                .catch((error) => {
                    UtilShowMessage.ToastShow(error, 'danger');
                });
        })
    }
    onCommnent = () => {
        this.componentWillMount()
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
               
            </View>
        )

    }
}
const styles = StyleSheet.create({
    button: {
        height: 40,
        bottom: '10%',
        borderColor: '#ffff',
        marginHorizontal: 15,
        alignSelf: 'flex-end',
        marginBottom: 10,
        right: 10
    },
});


export default ComponentsFormReviewStar = (FormReviewStar)
