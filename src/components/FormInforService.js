/**
 * @providesModule ComponentsFormInforService
 */
import React, { Component } from 'react';
import { StyleSheet, ImageBackground, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import Configs from 'ConfigsIndex';
import Icon from 'react-native-vector-icons/Ionicons';
import UtilAsyncStorage from 'UtilAsyncStorage';

const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height

export default class ComponentsFormInforService extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.infoService}>
                <ImageBackground source={{ uri: this.props.data ? UtilAsyncStorage.getImageStream(this.props.data.image) : null }}
                    style={styles.imageBackground}>
                    <TouchableOpacity onPress={this.props.goBack} style={styles.buttomBack}>
                        <Icon name="md-arrow-back" size={25} style={{ color: 'white' }} onPress={this.props.goBack} />
                    </TouchableOpacity>
                </ImageBackground>
                <View style={styles.infoDetail}>
                    <View>
                        <Text style={styles.textTitelSevice}>{this.props.data.title}</Text>
                    </View>
                    <View style={styles.infor}>
                        <Icon name="md-pin" size={14} style={styles.iconInfor} />
                        <Text style={styles.textInfor}>{this.props.data.address} </Text>
                    </View>
                    <View style={styles.infor}>
                        <Icon name="md-call" size={14} style={styles.iconInfor} />
                        <Text style={styles.textInfor}>{this.props.data.phone} </Text>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttomBack: {
        paddingTop: 5,
        paddingLeft: 15,
        flex: 1 / 10,
    },
    infoService: {
        flex: 1
    },
    infoDetail: {
        margin: 10,
    },
    infor: {
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageBackground: {
        width: widthW,
        height: heightW / 7 * 3
    },
    iconInfor: {
        color: Configs.backgroundColor,
        paddingRight: 10
    },
    textTitelSevice: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black'
    },
    textInfor: {
        color: 'black',
        fontSize: 14
    }
});
