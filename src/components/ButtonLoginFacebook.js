/**
 * @providesModule ComponentsButtonLoginFacebook
 */

import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Text, AsyncStorage } from 'react-native';
import Dimensions from 'Dimensions';
const FBSDK = require('react-native-fbsdk');
const {
    LoginManager,
    LoginButton,
    AccessToken,
    GraphRequest,
    GraphRequestManager
} = FBSDK;
import { Actions } from 'react-native-router-flux';
import ActionsAuthAuth from 'ActionsAuthAuth';
import { connect } from 'react-redux';
import Configs from 'ConfigsIndex';
import UtilAsyncStorage from 'UtilAsyncStorage'
import UtilShowMessage from 'UtilShowMessage'
const user_id = ''
const provider_login = ''
const full_name = ''
class ButtonLoginFacebook extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showPass: true,
            press: false,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width,
            password: '',
            username: 'rrrrr',
            user_id: '',
            provider_login: '',
            full_name: '',
            inputBorder: '#eded',
            defaultVal: ''

        };
    }
    //Create response callback.
    _responseInfoCallback = (error, result) => {
        if (error) {
            UtilShowMessage.ToastShow('Error fetching data: ' + error.toString(), 'danger')
        } else {
            UtilShowMessage.ToastShow('Result ID: ' + result.id + 'Result Name: ' + result.name + 'Result email: ' + result.email)
        }
    }
    fbAuth() {
        LoginManager.logInWithReadPermissions(['public_profile']).then(

            (result, _this) => {
                if (result.isCancelled) {
                    UtilShowMessage.ToastShow('Login cancelled', 'danger');
                } else {
                    user = AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            const { accessToken } = data
                            const infoRequest = new GraphRequest(
                                '/me?fields=name,picture',
                                null,
                                _responseInfoCallback = (error, result) => {
                                    if (error) {
                                        UtilShowMessage.ToastShow('Error fetching data: ' + error.toString(), 'danger');
                                    } else {
                                        UtilAsyncStorage.fetchAPI(
                                            Configs.hostname + '/api/login_api',
                                            {
                                                method: 'POST',
                                                headers:
                                                    {
                                                        'Accept': 'application/json',
                                                        'Content-Type': 'application/json',
                                                    },
                                                body: JSON.stringify(
                                                    {
                                                        api_type: "facebook",
                                                        api_id: result.id,
                                                        api_token: result.accessToken,
                                                        email: result.email,
                                                        full_name: result.name,
                                                        username: result.name,
                                                        avatar_img: result.picture.data.url
                                                    })
                                            }
                                        ).then((responseJson) => {
                                            if (responseJson['success_flg'] == true) {
                                                const user = {
                                                    isLogin: true,
                                                    user_id: responseJson['data']['user_id'],
                                                    provider_login: responseJson['data']['provider_login'],
                                                    full_name: responseJson['data']['full_name'],
                                                    email: responseJson['data']['email'] ? responseJson['data']['email'] : '',
                                                    phone: responseJson['data']['phone'] ? responseJson['data']['phone'] : '',
                                                    address: responseJson['data']['address'] ? responseJson['data']['address'] : '',
                                                    avatar_img: responseJson['data']['avatar_img'] ? responseJson['data']['avatar_img'] : '',
                                                    token: responseJson['data']['token']
                                                }

                                                this.props.onLogin(user)
                                                Actions.main();
                                            }
                                            else {
                                                UtilShowMessage.ToastShow(responseJson['errors'][0]['param'] + ': ' + responseJson['errors'][0]['msg'], 'danger')
                                            }
                                        })
                                    }
                                }
                            );
                            new GraphRequestManager().addRequest(infoRequest).start();

                        }
                    );

                }

            },
            function (error) {
                UtilShowMessage.ToastShow('Login fail with error: ' + error, 'danger');
            }
        );

    }

    render() {

        return (

            <View style={styles.container}>
                <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5} onPress={this.fbAuth.bind(this)}>
                    <Image
                        source={require('../public/images/icons/Facebook_Login_Button.png')}
                        style={styles.ImageIconStyle}
                    />
                    <Text style={styles.textButtonStyle}> Facebook </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'

    },
    FacebookStyle: {
        flexDirection: 'row',
        width: '80%',
        backgroundColor: '#485a96',
        borderWidth: .5,
        borderColor: '#485a96',
        height: 40,
        margin: 5,
        borderWidth: 1,
        borderRadius: 5,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonStyle: {
        color: "#fff",
        marginBottom: 4,
        marginRight: 20,

    },
    ImageIconStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',

    },
    ButtonLoginFacebook: {
        flexDirection: 'row',
        width: '80%',
        alignItems: 'center',
        borderWidth: .5,
        borderColor: '#fff',
        height: 30,
        borderRadius: 5,
        margin: 5,
        // textAlign: 'left'
    }
}
)
const mapStateToProps = (state, ownProps) => {
    return {
        username: state.auth.username
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (user) => {
            dispatch(ActionsAuthAuth.login(user));

        },
    }
}

export default ComponentsButtonLoginFacebook = connect(mapStateToProps, mapDispatchToProps)(ButtonLoginFacebook)