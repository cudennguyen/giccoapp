/**
 * @providesModule ComponentsFormViewComment
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux';
import { Thumbnail } from 'native-base';
import UtilFile from 'UtilFile';
import UtilShowMessage from "UtilShowMessage";
import UtilAsyncStorage from 'UtilAsyncStorage'
class FormViewComment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        };
    }
    componentWillMount() {
        const api = this.props.isService ? '/api/publish/integration/' : '/api/publish/package/';
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + api + this.props.packageId + "/comment?limit=" + this.props.limit,
            { method: 'GET' }
        ).then((responseJson) => {
            if (responseJson['success_flg'] == true) {
                this.setState({ data: responseJson.data })
            }
            else {
                UtilShowMessage.ToastShow('comment error!', 'danger')
            }
        })
    }
    convertDate(stringDate) {
        const date = new Date(stringDate);
        return date.toLocaleDateString("en-US");
    }
    render() {
        const data = [...this.state.data, ...this.props.comment]
        return (
            <FlatList
                data={data}
                keyExtractor={data._id}
                renderItem={({ item, index }) => {
                    return (
                        <View style={styles.container}>
                            <View style={styles.cardHeader}>
                                <Thumbnail small source={item.user.avatar_img ? { uri: UtilFile.checkFileExternal(item.user.avatar_img) } : require('../public/images/default_avatar.jpg')} />
                            </View>
                            <View style={styles.cardBody}>
                                <View style={styles.cardInfo}>
                                    <Text style={styles.userText}>{item.user ? item.user.full_name : "Anonymous"}</Text>
                                    <Text style={styles.contentTime}>{this.convertDate(item.created_date)}</Text>
                                </View>
                                <Text style={styles.contentText}>{item.comment}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        )
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10
    },
    cardHeader: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardBody: {
        flex: 9,
        flexDirection: 'column'
    },
    cardInfo: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black'
    },
    contentText: {
        flex: 1,
        fontSize: 14,
        textAlign: 'left',
        color: 'black'
    },
    contentTime: {
        fontSize: 12,
        fontStyle: 'italic',
        color: 'black'
    }

});
const mapStateToProps = (state, ownProps) => {
    return {
        comment: state.Package.comment
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onCurrentPosition: (comment) => {
            dispatch(ActionsPackage.currentReview(comment));

        },
    }
}

export default ComponentsFormViewComment = connect(mapStateToProps, mapDispatchToProps)(FormViewComment)