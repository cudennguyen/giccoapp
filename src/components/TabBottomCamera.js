
/**
 * @providesModule ComponentsTabBottomCamera
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
} from 'react-native';

import { Container, FooterTab, Content, Footer, Button, Text, Icon, Header } from 'native-base';
import Home from 'ComponentsHomeHome';
import Contact from 'ComponentsContactContact';
import Configs from 'ConfigsIndex';
import UseCamera from 'ComponentsUseCamera'
import FormMap from 'ComponentsFormMap'
import FormService from 'ComponentsFormService'
import { connect } from 'react-redux'
import ActionsPackagePackage from 'ActionsPackagePackage'
import { Actions } from 'react-native-router-flux';
class TabBottomCamera extends Component {
    constructor(props) {
        super(props);
        state = {

        }
    }
    componentWillMount(){
        this.props.onSelectTab('camera')
    }
    render() {
        const { iconHome } = styles;
        return (
            <Container>
                <Content contentContainerStyle={{ flex: 1 }}>
                    {this.props.tabSelect == 'camera' ?
                        <UseCamera packageId={this.props.packageId} /> : (this.props.tabSelect == 'map' ? <FormMap packageId={this.props.packageId} /> :
                            <FormService packageId={this.props.packageId} />)}
                </Content>
                <Footer>
                    <FooterTab style={{ backgroundColor: Configs.backgroundColor }}>
                        <Button vertical onPress={() => Actions.main()}>
                            <Icon name="home" style={styles.FooterIcon} />
                            <Text uppercase={false} style={styles.FooterText}>Home</Text>
                        </Button>
                        <Button style={this.props.tabSelect == 'camera' ? { opacity: 0.5 } : null} vertical onPress={() => this.props.onSelectTab('camera')}>
                            <Icon name="camera" style={styles.FooterIcon} />
                            <Text uppercase={false} style={styles.FooterText}>Camera</Text>
                        </Button>
                        <Button style={this.props.tabSelect == 'map' ? { opacity: 0.5 } : null} vertical onPress={() => this.props.onSelectTab('map')}>
                            <Icon name="md-map" style={styles.FooterIcon} />
                            <Text uppercase={false} style={styles.FooterText}>Map</Text>
                        </Button>
                        <Button style={this.props.tabSelect == 'services' ? { opacity: 0.5 } : null} vertical onPress={() => this.props.onSelectTab('services')}>
                            <Icon name="cog" style={styles.FooterIcon} />
                            <Text uppercase={false} style={styles.FooterText}>Services</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    Footer: {

        backgroundColor: Configs.backgroundColor
    },
    iconHome: {
        height: 30,
        width: 30
    },
    FooterText: {
        color: 'white', fontSize: 10
    },
    FooterIcon: {
        color: 'white', fontSize: 20
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        tabSelect: state.Package.tabSelect,
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onSelectTab: (tab) => {
            dispatch(ActionsPackagePackage.landmark(tab))
        }
    }
}

export default ComponentsTabBottomCamera = connect(mapStateToProps, mapDispatchToProps)(TabBottomCamera)
