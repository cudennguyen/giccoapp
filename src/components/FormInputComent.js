/**
 * @providesModule ComponentsInputComment
 */
import React, { Component } from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux';
import ActionsPackage from 'ActionsPackagePackage';
import UtilAsyncStorage from 'UtilAsyncStorage';
import UtilShowMessage from 'UtilShowMessage';
const widthW = Dimensions.get('window').width;
const heightW = Dimensions.get('window').height;

class FormInputComment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: ''
        }
    }
    sendComment = () => {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/package/comment',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(
                        {
                            user_id: this.props.user_id,
                            package_id: this.props.packageId,
                            comment: this.state.comment

                        })
                }
            ).then((responseJson) => {
                if (responseJson['success_flg'] == true) {
                    var date = new Date().getDate();
                    var month = new Date().getMonth() + 1;
                    var year = new Date().getFullYear();

                    day = (date + '-' + month + '-' + year);
                    comments = {
                        created_date: day,
                        comment: this.state.comment,
                        user: { full_name: this.props.full_name, }

                    }
                    this.props.onComment({ comments })
                    UtilShowMessage.ToastShow('Thanks your review of you, it is helpfull for me!')
                }
                else {
                    UtilShowMessage.ToastShow('Error is happen, please review again!', 'danger')
                }
            })
        })
    }
    componentWillMount() {
        if (this.props.comment)
            this.setState({
                comment: this.props.comment
            })

    }
    render() {
        return (
            <View style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                paddingRight: 10,
            }}>

                <TextInput
                    style={{ width: widthW * (6 / 7) }}
                    placeholder="Your Comment"
                    onChangeText={(comment) => this.setState({ comment })}
                    value={this.state.comment}
                />

                <View>
                    <TouchableOpacity style={styles.buttonSendComment}
                        onPress={this.sendComment}>
                        <Text>Send</Text>

                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSendComment: {
        top: '10%',
        bottom: '10%',
        borderColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'flex-end',
        width: 40,
        height: 30,
        marginBottom: 10,
        right: 10,


    },
});
const mapStateToProps = (state, ownProps) => {
    return {

        user_id: state.auth.user_id,
        full_name: state.auth.full_name,
        token: state.auth.token
    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onComment: (comment) => {
            dispatch(ActionsPackage.comment(comment));

        },
    }
}

export default ComponentsInputComment = connect(mapStateToProps, mapDispatchToProps)(FormInputComment)