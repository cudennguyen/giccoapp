/**
 * @providesModule ComponentsFormProfile
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';
import UtilAsyncStorage from 'UtilAsyncStorage'
import styles from 'PublicStylesStyleUser'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import Error from 'ComponentsHomeError'
import Configs from 'ConfigsIndex'
import UtilShowMessage from 'UtilShowMessage'
import { Actions } from 'react-native-router-flux'
class FormProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            full_name: this.props.user ? this.props.user.full_name : '',
            email: this.props.user ? this.props.user.email : '',
            phone: this.props.user ? this.props.user.phone : '',
            address: this.props.user ? this.props.user.address : '',
            errorusername: '',
            erroremail: '',
            errorphone: '',
            erroraddress: ''
        };
    }
    update = () => {
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/update_user',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(
                        {
                            user_meta: {
                                'full_name': this.state.full_name,
                                'phone': this.state.phone,
                                'address': this.state.address
                            },
                        })
                }
            ).then((responseJson) => {
                if (responseJson['success_flg'] == true) {
                    UtilShowMessage.ToastShow('Update infomation success!');
                    this.props.onChangeInfoUser({
                        full_name: this.state.full_name,
                        phone: this.state.phone,
                        address: this.state.address
                    })
                }
                else {
                    this.resetStateErrorMgs()
                    responseJson['errors'].map((item) => (
                        this.setState({ ['error' + item.param]: item.msg })
                    ))
                }
            })
        });

    }

    render() {
        const { user } = this.props
        return (
            <View style={styles.containerProfile}>
                <Error value={this.state.errorusername} />
                <View style={styles.inputGroups}>
                    <View style={styles.inputWrapper}>
                        <Icon name="md-contact" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorusername ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='User name'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(full_name) => this.setState({ full_name })}
                            value={this.state.full_name}
                            onSubmitEditing={() => this.phone.focus()} />
                    </View>
                    <Error value={this.state.erroremail} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-mail" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.erroremail ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Email'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            onSubmitEditing={() => this.name.focus()}
                            editable={false} />
                    </View>
                    <Error value={this.state.errorphone} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-phone-portrait" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.errorname ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Phone'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(phone) => this.setState({ phone })}
                            value={this.state.phone}
                            onSubmitEditing={() => this.address.focus()}
                            ref={(input) => this.phone = input} />
                    </View>
                    <Error value={this.state.erroraddress} />
                    <View style={styles.inputWrapper}>
                        <Icon name="md-home" size={25} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.erroraddress ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Address'
                            placeholderTextColor="#ffffff"
                            ref={(input) => this.address = input}
                            onChangeText={(address) => this.setState({ address })}
                            value={this.state.address} />

                    </View>

                </View>
                <View style={styles.bottom}>
                    <View style={styles.buttonUpdate}>
                        <TouchableOpacity style={styles.button}
                            onPress={this.update}>
                            <Text style={styles.buttonText}>
                                Update
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        user: state.auth
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onChangeInfoUser: (info) => {
            dispatch(ActionsAuthAuth.changeInfoUser(info));
        },
    }
}

export default ComponentsAvatarUser = connect(mapStateToProps, mapDispatchToProps)(FormProfile)