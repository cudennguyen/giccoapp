/**
 * @providesModule ComponentsHomeLogo
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
export default ComponentsHomeLogo = class Logo extends Component{
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logoImage}
                    source={require('../public/images/logo.png')} />

            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'

    },
    logoText: {
        marginVertical: 15,
        fontSize: 18,
        color: 'rgba(255, 255, 255, 0.7)'
    },
    logoImage:{
        width: 255/2.5,
        height: 255/2.5
        },
})
