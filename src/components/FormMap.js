/**
 * @providesModule ComponentsFormMap
 */
import React, { Component } from 'react'
import {
    View, Text, StyleSheet, Image, WebView, ToastAndroid,
    ScrollView,
    Animated,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Modal, TouchableHighlight, PanResponder
} from 'react-native'
import Configs from 'ConfigsIndex'
import { connect } from 'react-redux'
import UtilShowMessage from 'UtilShowMessage'
import UtilAsyncStorage from 'UtilAsyncStorage'
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;
const widthW = Dimensions.get('window').width
const heightW = Dimensions.get('window').height
class FormMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            position: 0,
            starCount: 4,
            interval: null,
            lastLandmark: '',
            audio: '',
            markers: [],
            dataSource: [],
            region: null,
        };
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderGrant: this._onPanResponderGrant.bind(this),
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove.bind(this),
        })
    }
    _toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }
    componentWillMount() {
        let region
        this.props.landmarkId ?
            UtilAsyncStorage.getStorageHeader().then((headers) => {
                fetch(Configs.hostname + '/api/publish/landmark/' + this.props.landmarkId,
                    {
                        method: 'GET',
                        headers: headers,
                    }).then((response) => response.json())
                    .then((responseJson) => {
                        if (responseJson.success_flg == true) {
                            coordinate = responseJson.data.lat_long
                            coordinate = coordinate.split(',');
                            region = {
                                latitude: parseFloat(coordinate[0]),
                                longitude: parseFloat(coordinate[1]),
                                latitudeDelta: 0.02,
                                longitudeDelta: 0.02,
                            }
                            imageArray = []
                            for (var i = 0, len = responseJson.data.images.length; i < len; i++) {
                                imageArray.push({ url: Configs.hostname + '/packages/' + responseJson.data.images[i] })
                            }
                            imageArray.splice(Configs.LIMIT_IMAGE)
                            this.setState({
                                ...this.state,
                                audio: Configs.hostname + '/packages/' + responseJson.data.audio,
                                landmarkImage: '/packages/' + responseJson.data.image_thumb,
                                data: responseJson.data,
                                region: region,
                                dataSource: imageArray,
                                lastLandmark: this.props.landmarkId
                            })
                        }
                        else {
                            UtilShowMessage.ToastShow('error data respond landmard', 'danger')

                        }

                    })
                    .catch((error) => {
                        UtilShowMessage.ToastShow(error, 'danger')
                    });
            }) : fetch(Configs.hostname + '/api/publish/package/marker/' + this.props.packageId,
                {
                    method: 'GET'

                }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson['success_flg'] == true) {
                        coordinate = responseJson.results[0].lat_long
                        coordinate = coordinate.split(',');
                        region = {
                            latitude: parseFloat(coordinate[0]),
                            longitude: parseFloat(coordinate[1]),
                            latitudeDelta: 0.02,
                            longitudeDelta: 0.02,

                        }
                        this.setState({
                            markers: responseJson.results,
                            region: region,
                            audio: '',
                        })
                    }
                    else {
                        UtilShowMessage.ToastShow(responseJson['message'], 'danger')
                    }
                })
                .catch((error) => {
                    UtilShowMessage.ToastShow(error, 'danger')
                });

        this.index = 0;
        this.animation = new Animated.Value(0);
    }
    componentDidMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
                });
            }, 5000)
        });
        // We should detect when scrolling has stopped then animate
        // We should just debounce the event listener here
        this.animation.addListener(({ value }) => {
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= this.state.markers.length) {
                index = this.state.markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }
        });
    }
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
        UtilAsyncStorage.getStorageHeader().then((headers) => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/publish/landmark/rating',
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(
                        {
                            landmark_id: this.props.landmarkId,
                            star_number: this.state.starCount

                        })
                }
            ).then((responseJson) => {
                if (responseJson['success_flg'] == true) {
                    UtilShowMessage.ToastShow('Thanks your review of you, it is helpfull for me!')
                }
                else {
                    UtilShowMessage.ToastShow("Error is happen, please review again!", 'danger')
                }
            })
        })
    }
    _onPanResponderGrant(event, gestureState) {
        if (event.nativeEvent.locationX === event.nativeEvent.pageX) {
            this.setState({ showModal: false, position: 0 })
        }
    }
    _onPanResponderMove(event, gestureState) {
        if (gestureState.moveY - gestureState.y0 > 300) {
            this.setState({ showModal: false, position: 0 })
        }
    }
    render() {
        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
                (index - 1) * CARD_WIDTH,
                index * CARD_WIDTH,
                ((index + 1) * CARD_WIDTH),
            ];
            const scale = this.animation.interpolate({
                inputRange,
                outputRange: [1, 2.5, 1],
                extrapolate: "clamp",
            });
            const opacity = this.animation.interpolate({
                inputRange,
                outputRange: [0.35, 1, 0.35],
                extrapolate: "clamp",
            });
            return { scale, opacity };
        });
        return (
            <View style={styles.container}>
             
            </View>
        )
    }
}
const styles = StyleSheet.create({
    map: {
        // ...StyleSheet.absoluteFillObject,
        height: heightW / 7 * 5,
        flex: 9
    },
    slideshow: {
        marginTop: 10,
        marginLeft: 1,
        marginRight: 1,
    },
    formStarRating: {
        flex: 2 / 3,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingRight: 10
    },
    formSlideShow: {
        backgroundColor: 'rgba(52, 52, 52, 0.8)', height: heightW
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    container: {
        flex: 1, flexDirection: 'column'
    }

})

const mapStateToProps = (state, ownProps) => {
    return {
        currentPosition: state.Audio.currentPosition,
        sliding: state.Audio.sliding,
        token: state.auth.token,
        landmarkId: state.Package.landmarkId,

    };
}
const mapDispatchToProps = (dispatch) => {
    return {

        onCurrentPosition: (currentPosition) => {
            dispatch(ActionsAudioAudio.currentPosition(currentPosition));


        },
    }
}

export default ComponentsFormMap = connect(mapStateToProps, mapDispatchToProps)(FormMap)