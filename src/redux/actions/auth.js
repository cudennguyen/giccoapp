export default login = (user_id, username, provider_login, full_name, token) => {
    return {
        type: 'LOGIN',
        user_id: user_id,
        username: username,
        provider_login: provider_login,
        full_name: full_name,
        token: token
    };
};

export const logout = () => {
    return {
        type: 'LOGOUT'
    };
};

export const signup = (username, password) => {
    return (dispatch) => {
    };
};