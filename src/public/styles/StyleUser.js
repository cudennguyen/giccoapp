/**
 * @providesModule PublicStylesStyleUser
 */

import { StyleSheet } from 'react-native';
import Configs from 'ConfigsIndex';
export default PublicStylesStyleUser = StyleSheet.create({
    container: {
        flex: 13,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Configs.backgroundColor,

    },
    containerProfile:{
        flex: 1,
    },
    inputGroups: {
        width: '100%',
        height: '100%',
        flex: 13,
    },
    formConenctwith: {
        width: '100%',
        flex: 4,
        paddingTop:50
    },
    buttonNetSocial: {
        flexDirection: 'row',
    },
    inputBox: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        width: '90%',
        height: 40,
        paddingLeft: 45,
        borderWidth: 1,
        borderRadius: 20,
        marginHorizontal: 20,
        fontSize: 16,
        color: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttons: {
        flex: 2,
        width: '100%',
    },
    buttonSignIn: {
        flex: 3,
        top: '10%'
    },
    button: {
        // flex: 1,
        width: '90%',
        height: 40,
        top: '10%',
        bottom: '10%',
        backgroundColor: '#ffff',
        borderColor: '#ffff',
        borderWidth: 1,
        borderRadius: 20,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',


    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: Configs.backgroundColor,
        textAlign: 'center'
    },
    btnEye: {
        position: 'absolute',
        right: '8%',
        top: '12%',
    },
    iconEye: {
        width: 25,
        height: 25
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
        color: Configs.fontColor
    },
    inputWrapper: {
        width: '100%',
        flex: 1,
        marginVertical: 4
    },
    signupbutton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500'
    },
    signupTextCont: {
        flex: 1,
        width: '100%',
        top: '10%',
        alignItems: 'center',
        paddingVertical: 16,
    },
    textWithDivider: {
        color: '#bfbfbf',
        marginVertical: 10,
        paddingHorizontal: 10
    },
    buttonUpdate: {
        flex: 1,
        top: '10%'
    },
    bottom:{
        flex: 3
    }

})