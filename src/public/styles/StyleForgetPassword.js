/**
 * @providesModule PublicStylesStyleForgetPassword
 */

import { StyleSheet } from 'react-native';
import Configs from 'ConfigsIndex';
export default PublicStylesStyleForgetPassword = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Configs.backgroundColor
    },
    logo: {
        flex: 2
    },
    forgetPassword: {
        flex: 4
    },
    textContent: {

        color: '#ffffff',
        fontSize: 16,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },

    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 95,

    },
    inputWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginVertical: 4
    },
    buttonSignIn: {
        flex: 1,
        top: '10%'
    },
    inputBox: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        width: '90%',
        height: 40,
        paddingLeft: 45,
        borderWidth: 1,
        borderRadius: 20,
        marginHorizontal: 20,
        fontSize: 16,
        color: '#ffffff',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',

    },
    button: {
        width: '90%',
        height: 40,
        top: '10%',
        bottom: '10%',
        backgroundColor: '#ffff',
        borderColor: '#ffff',
        borderWidth: 1,
        borderRadius: 20,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: Configs.backgroundColor,
        textAlign: 'center'
    },

})