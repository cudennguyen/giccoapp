/**
 * @providesModule ActionsAuthAuth
 */
import Configs from 'ConfigsIndex';
import UtilAsyncStorage from 'UtilAsyncStorage';
export default ActionsAuthAuth = {
    login: (user) => {
        return {
            type: 'LOGIN',
            user: user
        };
    },
    logout: () => {
        UtilAsyncStorage.setLogout();
        return {
            type: 'LOGOUT'
        };
    },
    signup: (username, password) => {
        return (dispatch) => {
        };
    },
    changeAvatar: (avatar_img) => {
        UtilAsyncStorage.updateStorageAvatar(avatar_img);
        return {
            type: 'CHANGE_AVATAR',
            avatar_img: avatar_img
        }
    },
    changeInfoUser: (info) => {
        UtilAsyncStorage.updateStorageUser(info);
        return {
            type: 'CHANGE_INFOR_USER',
            user: info
        }
    },
    asyncStorage: (user) => {
        return {
            type: 'ASYNC_STORAGE',
            user: user
        }
    },
    getAsyncStorage: () => {
        return (dispatch) => {
            UtilAsyncStorage.getStorageUser().then(data => {
                dispatch(ActionsAuthAuth.asyncStorage(data));
            });
        }
    }
}
