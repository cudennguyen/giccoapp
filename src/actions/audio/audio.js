/**
 * @providesModule ActionsAudioAudio
 */
export default ActionsAudioAudio = {
    currentPosition: (currentPosition) => {
        return {
            type: 'CURRENT_POSITION',
            currentPosition: currentPosition,
        };
    },
    sliding: (sliding) => {
        return {
            type: 'SLIDING',
            sliding: sliding
        }
    },
    urlAudio: (url) => {
        return {
            type: 'AUDIO',
            url: url
        }
    },
    playSong: (song) => {
        return {
            type: 'PLAYSONG',
            song: song
        }
    }

}
