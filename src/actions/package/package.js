/**
 * @providesModule ActionsPackagePackage
 */
export default ActionsPackagePackage = {
    packageAuth: (auth) => {
        return {
            type: 'PACKAGE_AUTH',
            auth: auth,
        };
    },
    onAddtoCart: (id,name,quality) => {
        return {
            type: 'ADD_TO_CART',
           product:{id:id,name:name,quality:quality}
        };
    },
    tabDetail: (discription) => {
        return {
            type: 'TAB_DETAIL',
            discription: discription
        }
    },
    comment: (comment) => {
        return {
            type: 'COMMENT',
            comment: comment
        }
    },
    catogorySearch: (classify) => {
        return {
            type: 'CLASSIFY',
            classify: classify
        }
    },
    landmark: (tabSelect, landmarkId) => {
        return {
            type: 'LANDMARK',
            tabSelect: tabSelect,
            landmarkId: landmarkId
        }

    },
    selectedTab: (tab) => {
        return {
            type: 'SELECTEDTAB',
            tab: tab

        }
    },
    typeIntegration: (typeIntegration, title, idIntegration) => {
        return {
            type: 'TYPEINTEGRATION',
            typeIntegration: typeIntegration,
            title: title,
            idIntegration: idIntegration
        }
    },
    searchPackage: (search) => {
        return {
            type: 'SEARCHPACKAGE',
            keySearch: search
        }
    },
    locationIntegration: (region, locationIntegration, titleMarker) => {
        return {
            type: 'LOCATIONINTEGRATION',
            region: region,
            locationIntegration: locationIntegration,
            titleMarker: titleMarker

        }
    },
    integrationChoosePackage: (arrayPackageIntegration) => {
        return {
            type: 'INTEGRATIONCHOOSEPACKAGE',
            arrayPackageIntegration: arrayPackageIntegration

        }
    },
    saveTempt: (typeIntegration, file, name, introduction, address, phone) => {
        return {
            type: 'SAVETEMPT',
            file: file,
            name: name,
            introduction: introduction,
            address: address,
            phone: phone
        }

    },
    showError: (erroraddress, errorphone, errorintroduce, errorlat_long, errorpackage, errortitle) => {
        return {
            type: 'SHOWERROR',
            erroraddress: erroraddress,
            errorphone: errorphone,
            errorintroduce: errorintroduce,
            errorlat_long: errorlat_long,
            errorpackage: errorpackage,
            errortitle: errortitle,
        }
    },
    showErrorImage: (errorimage) => {
        return {
            type: 'SHOWERRORIMAGE',
            errorimage: errorimage
        }
    }

}
