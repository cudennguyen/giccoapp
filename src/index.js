/**
 * @providesModule SrcIndex
 */

import { createStore, applyMiddleware } from 'redux';
import rootReducer from 'ReducersAuthIndex';
import thunk from 'redux-thunk';
const store = createStore(rootReducer, applyMiddleware(thunk));
 
export default SrcIndex =  store;