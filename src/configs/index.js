/**
 * @providesModule ConfigsIndex
 */

export default ConfigsIndex = {
    backgroundColor: '#36A2CD',
    loginBackground: '#8B8378',
    swapBackgroundColor: '#ffffff',
    fontColor: '#ffffff',
    visibleColor: '#E3E2E0',
    logoImage: require('../public/images/logo.png'),
    hostname: 'https://giccowebhost20181003115039.azurewebsites.net',
    BOUGHT: 0,
    BUY: 1,
    LIMIT_DEFAULT: 6,
    PAGE_DEFAULT: 1,
    ZOOM_LATITUDE: 0.0922,
    ZOOM_LONGITUDE: 0.0421,
    LIMIT_IMAGE: 10,
    apiImage: '/api/resize?'
}