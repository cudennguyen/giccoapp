/**
 * @providesModule PagesApplication
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from 'PagesLogin';
import Secured from 'PagesSecured';
import { createStore } from 'redux'

class Application extends Component {
    render() {
        if (this.props.isLoggedIn) {
            return <Secured />;
        } else {
            return <Login />;
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    };
}

export default PagesApplication = connect(mapStateToProps)(Application);