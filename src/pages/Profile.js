/**
 * @providesModule PagesProfile
 */

import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Dimensions } from 'react-native'

import ChangePassword from 'ComponentsChangePassword'
import AvatarUser from 'ComponentsAvatarUser'
import Icon from 'react-native-vector-icons/Ionicons'
import FormProfile from 'ComponentsFormProfile'
import Configs from 'ConfigsIndex'
import { connect } from 'react-redux'
import Header from 'ComponentsPackageHeaderItems'
import UtilAsyncStorage  from 'UtilAsyncStorage'
const heightW = Dimensions.get('window').height
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changePassword: false,
            status: false
        }
        this.toggleChangePassword = this.toggleChangePassword.bind(this);
    }
    toggleChangePassword() {
        this.setState({
            changePassword: !this.state.changePassword
        });
        this.scrollView.scrollTo({ x: 0, y: heightW * 0.5 });

    }
    componentWillMount() {
        UtilAsyncStorage.getStorageUser().then((data) => {
            this.setState({              
                provider_login:data.provider_login
            })
          })    
    }
    render() {
        return (
            <View style={styles.container}>
                <Header title="My Profile"  cart={false}
                />
                <ScrollView
                    ref={(scrollView) => {
                        this.scrollView = scrollView;
                    }}
                    showsVerticalScrollIndicator={false}
                    style={styles.container}
                >

                    <View style={styles.avatar}>
                        <AvatarUser />
                    </View>

                    <View style={{
                        marginTop: 5,
                        borderBottomColor: 'white',
                        borderBottomWidth: 1,
                    }} />
                    <View style={styles.formProfile}>
                        <FormProfile />
                    </View>

                    <View style={{ paddingTop: 30, paddingBottom: 20 }}>
                        {this.state.provider_login == 'local' ?

                            <TouchableOpacity onPress={this.toggleChangePassword}>
                                <View style={styles.textChangePassword}>
                                    <Text style={styles.changePassword}>Change Password</Text>
                                    <View style={styles.iconadd}>
                                        {this.state.changePassword ?
                                            <Icon name="md-remove" size={25} style={styles.inlineImg} />
                                            :
                                            <Icon name="md-add" size={25} style={styles.inlineImg} />
                                        }
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : null}
                        {this.state.changePassword ?
                            <View style={{ flex: 1, paddingBottom: 20, paddingTop: 20 }}>
                                <ChangePassword />
                            </View>
                            : null}

                    </View>
                </ScrollView>
            </View>

        )

    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.backgroundColor,
    },
    avatar: {
        flex: 2,
        paddingLeft: 10

    },
    iconadd: {
        alignItems: 'center',
        marginRight: 5,
    },
    formProfile: {
        flex: 6
    },
    changePassword: {
        fontSize: 16,
        marginLeft: 10,
        marginRight: 5,
        color: Configs.backgroundColor,
        alignItems: 'center',
    },
    textChangePassword: {
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: Configs.backgroundColor,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        borderColor: '#ffff',
        borderWidth: 1,
        alignItems: 'center',

    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        provider_login: state.auth.provider_login,

    };
}

export default PagesProfile = connect(mapStateToProps)(Profile)


