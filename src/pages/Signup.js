/**
 * @providesModule PagesSignup
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import Logo from 'ComponentsHomeLogo';
import Form from 'ComponentsForm';
import FormRegister from 'ComponentsFormRegister';
import Configs from 'ConfigsIndex';
import { Actions } from 'react-native-router-flux';
export default PagesSignup = class Signup extends Component {
    goBack() {
        Actions.login();
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Signup</Text>
                <FormRegister />
                <View style={styles.signupTextCont}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Text style={styles.signupbutton}>Sign in</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Configs.backgroundColor
    },
    header: {
        flex: 2,
        fontSize: 24,
        color: '#ffffff',
        marginTop: 50
    },

    signupTextCont: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingVertical: 16,

    },
    signupText: {
        color: 'rgba(255, 255, 255, 0.6)',
        fontSize: 16
    },
    signupbutton: {
        color: '#ffffff',
        fontSize: 14
    }
})