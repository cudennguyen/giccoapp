/**
 * @providesModule PagesUsePackage
 */

import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import TabBottomCamera from 'ComponentsTabBottomCamera'
import Configs from 'ConfigsIndex';

const { height } = Dimensions.get('window');

export default PagesUsePackage = class UsePackage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            text: '',
            selectedTab: 'home'
        };
    }
    closeControlPanel = () => {
        this.drawer.close();
    };
    openControlPanel = () => {
        this.drawer.open();
    };
    render() {
        const {
            container, header, imgBack, row1, imgSearch
        } = styles;
        return (

            <View style={container}>
                
                <TabBottomCamera packageId={this.props.packageId} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.visibleColor,
    }
});
