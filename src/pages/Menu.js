/**
 * @providesModule PagesMenu
 */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,TouchableOpacity
} from 'react-native';
import { Header, Thumbnail, Button, Right, List, ListItem, Left } from 'native-base'
import Configs from 'ConfigsIndex';
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import ActionsAuthAuth from 'ActionsAuthAuth'
import UtilAsyncStorage from 'UtilAsyncStorage'
import {
  Content,
  Icon,
} from "native-base";
const datas = [
  {
    name: "My profile",
    route: "Myprofile",
    icon: "md-contact",
    bg: "#C5F442",
    isLogin: true
  },
  {
    name: "Setting",
    route: "Setting",
    icon: "md-settings",
    bg: "#C5F442",
    isLogin: true
  },
  {
    name: "Sign out",
    route: "Signout",
    icon: "md-exit",
    bg: "#C5F442",
    isLogin: true
  },
  {
    name: "Login",
    route: "Login",
    icon: "md-exit",
    bg: "#C5F442",
    isLogin: false
  },
];
const heightW = Dimensions.get('window').height
class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      data: [],
      active: 0
    };
  }
  myPackage = () => {
    Actions.MyPackage({ titlePackage: 'My Package' })
  }
  onPressMenu = (route, id, name, length) => {
    if(length!=0)
    {
      this.setState({
        active :length
      })
    }else{
       switch (route) {
      case "MyPackage": Actions.MyPackage({ titlePackage: 'My Package' }); break;
      case "Signout": this.props.onSignOut(); break;
      case "Setting": ; break;
      case "Myprofile": Actions.profile(); break;

      case "Login": Actions.login(); break;
      default:
        Actions.packagelist({
          api: '/api/mobile/products/cate/' + id,
          titlePackage: name
        });
        break;
    }
    }
   
  }
  componentWillMount() {
    UtilAsyncStorage.fetchAPI(Configs.hostname + '/api/mobile/categories/tree', { method: 'GET' }).then((responseJson) => {
      this.setState({
        data: responseJson.concat(datas)
      })
    })
  }
  render() {
    console.log("!",this.state.data)
    const { user } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header
          iosBarStyle='light-content'
          androidStatusBarColor={Configs.backgroundColor}
          style={styles.header}
        >
          <View style={styles.infoUser}>
            <Thumbnail
              source={user.avatar_img ? {
                uri: user.avatar_img
              } : require('../public/images/default_avatar.jpg')}
            />

            <View style={styles.textInfoUser}>
              <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white' }}>{user.full_name}</Text>
              <Text style={{ fontSize: 12, color: 'white', marginBottom: 10 }}>{user.email}</Text>
            </View>
          </View>

          <Right>
            <Button
              transparent
              onPress={this.props.closeDrawer}
            >
              <Icon name='arrow-forward' style={{ color: 'white' }}></Icon>
            </Button>
          </Right>

        </Header>
        <Content bounces={false} style={styles.content}>
          <List>
            {this.state.data.map((item, index) => (
              user.isLogin == item.isLogin || item.isLogin == null ?
                <ListItem iconLeft style={{ borderBottomColor: 0  }}
                
                  key={index}
                >
                  <Icon
                    active
                    name={item.icon}
                    style={styles.iconButton}

                  />
                  <View > 
                    <TouchableOpacity   onPress={() => this.onPressMenu(item.route, item.id, item.name,item.childItems&&item.childItems.length>0?item.id:0)}><Text style={{
                    color: 'black',
                  }}>{item.name}</Text></TouchableOpacity>
                    
                 
                  {item.id == this.state.active?<View style={{marginLeft :20}}>
                    {item.childItems? <List>
                    {item.childItems.map((items, index) => (
                   
                      <TouchableOpacity  onPress={() => this.onPressMenu(items.slug, items.id, items.name,0)}><Text>{items.name}</Text></TouchableOpacity>
                    ))}
                    </List>:null}
                   
                  </View>:null}
                  </View>
                </ListItem>
                : null
            ))}
          </List>
        </Content>
      </View>
    );

  }
}


const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    marginLeft: 20,
    height: 40,
    color: Configs.backgroundColor
  },
  infoUser: {
    flexDirection: 'row',
    alignContent: 'center',
    paddingTop: 10
  },
  textInfoUser: {
    justifyContent: 'center',
    paddingLeft: 10
  },
  header: {
    height: heightW / 7,
    backgroundColor: Configs.backgroundColor,
  },
  content: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 20
  },
  iconButton: {
    color: Configs.backgroundColor,
    fontSize: 26,
    paddingRight: 10
  },
  buttonStyle: {
    justifyContent: 'flex-start',
    borderRadius: 5,
    width: '100%',
    marginBottom: 10,
    backgroundColor: Configs.backgroundColor
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    onSignOut: () => {
      dispatch(ActionsAuthAuth.logout());
    }
  }
}

export default PagesMenu = connect(mapStateToProps, mapDispatchToProps)(Menu)
