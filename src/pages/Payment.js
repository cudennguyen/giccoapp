/**
 * @providesModule Payment
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions
} from 'react-native'
const { width } = Dimensions.get('window');
import Error from 'ComponentsHomeError'
import Header from 'ComponentsPackageHeaderItems';
import { Container, Item, Picker, Label, Radio, Input, Left, Body, Right, Button, Icon, Title, Text, Content, Card, CartItem, CardItem, Footer } from 'native-base';
import Configs from 'ConfigsIndex'
import UtilShowMessage from 'UtilShowMessage';
import UtilAsyncStorage from 'UtilAsyncStorage'
import { Actions } from 'react-native-router-flux';
export default Payment = class Payment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            error_email: '',
            radio: "Free",
            countries: [],
            provinces: [],
            district: [],
            existingShippingAddresses: []
        };
    }
    onValueChange(value) {
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/countries/" + value + "/states-provinces",
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((responseJson) => {
                this.setState({
                    countryID: value,
                    provinces: responseJson
                });
            })
    }
    onValueChange2(value) {
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/states-provinces/" + value + "/districts",
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((responseJson) => {
                this.setState({
                    provincesID: value,
                    district: responseJson
                });
            })
    }
    componentWillMount() {
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/mobile/checkout/shipping",
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((responseJson) => {
                console.log("Ress", responseJson)
                this.setState({
                    existingShippingAddresses: responseJson.existingShippingAddresses
                })
            })
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/countries?shippingEnabled=true",
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((responseJson) => {
                this.setState({
                    countries: responseJson
                })
            })

    }
    onValueChange3(value) {
        this.setState({
            districtID: value
        });
    }
    payment() {
        this.state.new == true ?
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/mobile/checkout/shipping',
                {
                    method: 'POST',
                    headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(
                        {
                            "shippingMethod": this.state.radio,
                            "shippingMethods": [
                                {
                                    "key": this.state.radio,
                                    "value": this.state.radio
                                }
                            ],
                            "newAddressForm": {
                                "contactName": this.state.contact,
                                "phone": this.state.phone,
                                "addressLine1": this.state.address,
                                "addressLine2": this.state.address,
                                "stateOrProvinceId": this.state.provincesID,
                                "districtId": this.state.districtID,
                                "countryId": this.state.countryID,
                                "city": this.state.city,
                                "zipCode": this.state.postCode
                            }
                        })
                }).then((responseJson) => {
                    this.payment2()
                    UtilShowMessage.ToastShow('Thanh toán thành công')
                    Actions.main()
                })
            :
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/api/mobile/checkout/shipping',
                {
                    method: 'POST',
                    headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(
                        {
                            "shippingAddressId": this.state.userAddressId,
                            "shippingMethod": this.state.radio
                        })
                }).then((responseJson) => {
                    this.payment2()
                    UtilShowMessage.ToastShow('Thanh toán thành công')
                    Actions.main()
                })

    }
    payment2() {
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/mobile/cod-checkout",
            {
                method: 'POST',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify()
            }).then((responseJson) => {
                console.log("21212")
            })
    }
    render() {
        return (
            <Container>
                <Header title='Thanh toán' cart={false} />
                <Content style={styles.content}>
                    <Label style={{ textAlign: 'center' }}>
                        Tổng đơn :{this.props.count}
                    </Label>
                    {this.state.existingShippingAddresses ?
                        <View style={{ flexDirection: 'row', marginLeft: 10 }}><Text>Using new address</Text>
                            <Radio
                                onPress={() => this.setState({ new: !this.state.new })}
                                color={"#f0ad4e"}
                                selectedColor={"#5cb85c"}
                                style={{ marginLeft: 10 }}
                                selected={this.state.new}
                            /></View>
                        : null}
                    {this.state.new == true ? <View>
                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                Contact name
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Input
                                value={this.state.contact}
                                onChangeText={(text) => this.setState({ contact: text })} />
                        </Item>
                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                Country
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Picker
                                notek
                                mode="dropdown"
                                style={{ width: width }}
                                selectedValue={this.state.countryID}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                {this.state.countries.map((item, index) =>
                                    <Picker.Item label={item.name} value={item.id} />
                                )}
                            </Picker>
                        </Item>

                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                State or Province
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Picker
                                note
                                mode="dropdown"
                                style={{ width: width }}
                                selectedValue={this.state.provincesID}
                                onValueChange={this.onValueChange2.bind(this)}
                            >
                                {this.state.provinces.map((item, index) =>
                                    <Picker.Item label={item.name} value={item.id} />
                                )}
                            </Picker>
                        </Item>

                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                District
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Picker
                                note
                                mode="dropdown"
                                style={{ width: width }}
                                selectedValue={this.state.districtID}
                                onValueChange={this.onValueChange3.bind(this)}
                            >
                                {this.state.district.map((item, index) =>
                                    <Picker.Item label={item.name} value={item.id} />
                                )}
                            </Picker>
                        </Item>

                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                City
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Input
                                value={this.state.city}
                                onChangeText={(text) => this.setState({ city: text })} />
                        </Item>

                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                Postal Code
                          </Label>
                            <Error value={this.props.po} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Input
                                value={this.state.postCode}
                                onChangeText={(text) => this.setState({ postCode: text })} />
                        </Item>
                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                Address
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Input
                                value={this.state.address}
                                onChangeText={(text) => this.setState({ address: text })} />
                        </Item>
                        <View style={styles.viewItem}>
                            <Label style={styles.label}>
                                Phone
                          </Label>
                            <Error value={this.props.errortitle} />
                        </View>
                        <Item stackedLabel style={styles.item}>
                            <Input
                                value={this.state.phone}
                                onChangeText={(text) => this.setState({ phone: text })} />
                        </Item></View> :
                        <View>
                            {this.state.existingShippingAddresses.map((item, index) =>
                                <Item selected={true}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Radio
                                            onPress={() => this.setState({ userAddressId: item.userAddressId })}
                                            color={"#f0ad4e"}
                                            selectedColor={"#5cb85c"}
                                            selected={this.state.userAddressId == item.userAddressId}
                                        />
                                        <Text> {item.contactName},
                                        {item.addressLine1}, {item.districtName},
                                         {item.stateOrProvinceName}, {item.countryName},
                                          {item.phone} </Text>
                                    </View>

                                </Item>
                            )}
                        </View>
                    }
                    <Item selected={true}>
                        <Left>
                            <Text>Free</Text>
                        </Left>
                        <Right>
                            <Radio
                                onPress={() => this.setState({ radio: 'Free' })}
                                color={"#f0ad4e"}
                                selectedColor={"#5cb85c"}
                                selected={this.state.radio == 'Free'}
                            />
                        </Right>
                    </Item>
                    <Item selected={true}>
                        <Left>
                            <Text>Stander               + $2.00</Text>
                        </Left>
                        <Right>
                            <Radio
                                onPress={() => this.setState({ radio: 'Standard' })}
                                color={"#f0ad4e"}
                                selectedColor={"#5cb85c"}
                                selected={this.state.radio == 'Standard'}
                            />
                        </Right>

                    </Item>
                    <Item>
                        <TouchableOpacity style={{ width: 120, borderRadius: 5, height: 30, marginTop: 10, marginBottom: 5, marginLeft: 'auto', backgroundColor: "lightblue", right: 20, alignItems: 'center' }}
                            onPress={() => { this.payment() }}>
                            <Text>Thanh toán</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 120, borderRadius: 5, height: 30, marginTop: 10, marginBottom: 5, marginLeft: 'auto', backgroundColor: "lightblue", right: 20, alignItems: 'center' }}
                            onPress={() => { this.payment2() }}>
                            <Text>Thanh toán2</Text>
                        </TouchableOpacity>
                    </Item>

                </Content>
            </Container>
        )
    }
}
const imageWidth = width * 0.2;
const imageHeight = 70;
const styles = StyleSheet.create({
    content: {
        flex: 10,
        backgroundColor: 'white'
    },
    label: {
        color: 'black',
        fontSize: 14,
        paddingLeft: 10
    },
    item: {
        flex: 1,
        flexDirection: 'column'

    },
    viewItem: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
});


