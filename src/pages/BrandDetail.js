/**
 * @providesModule PagesBrandDetail
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Dimensions,
    Image,
    TouchableOpacity
} from 'react-native';
import Header from 'ComponentsPackageHeaderItems';
import Configs from 'ConfigsIndex';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import ActionsPackagePackage from 'ActionsPackagePackage'
import UtilAsyncStorage from 'UtilAsyncStorage'
import { Card, CardItem, Body } from 'native-base'
import UtilShowMessage from "UtilShowMessage";
let page = Configs.PAGE_DEFAULT;
const { width } = Dimensions.get('window');
class BrandDetail extends Component {
    constructor(props) {
        super(props);
        isLoadMore = true;
        this.state = {
            data: []
        };
    }

    componentWillMount() {
    
                    this.setState({ data: this.props.data })
                   
            
    }
    goToDetail(id) {
        this.props.onTabName('introduction')
        Actions.packagedetail({ idPackage: id, titlePackage: id });
    }

    onAddtoCart = (item) => {
        // Actions.UsePackage({ packageId: packageId })
         this.props.onAddtoCart(item.id,item.name,1)
    }
    render() {    
        return (
            <View style={styles.container}>
                <Header title={this.props.titlePackage} />
                <FlatList
                    ref="list"
                    refreshing={this.state.refresh}
                    numColumns={2}
                    inverted={false}
                    contentContainerStyle={{ margin: 5 }}
                    horizontal={false}
                    onEndReachedThreshold={0.2}                 
                    showsVerticalScrollIndicator={false}
                    style={styles.content}
                    data={this.state.data}
                    renderItem={
                        ({ item }) =>
                            <View style={styles.package}>
                                <Card>
                                    <CardItem cardBody button
                                        onPress={() => item.package_id ? this.goToDetail(item.package_id._id) : null}
                                        style={{ flexDirection: 'column' }}> 
                                        <Image style={styles.imageStyle} source={{ uri: Configs.hostname + item.thumbnailUrl }} />
                                       <Text numberOfLines={1} style={styles.contentBoxText}>{item.name}</Text>   
                                       {item.oldPrice? <Text numberOfLines={1} style={styles.contentBoxText2}>{item.oldPrice}$</Text> :null}                             
                                       
                                        <Text numberOfLines={1} style={styles.contentBoxText}>{item.price}$</Text>                                       
                                    </CardItem> 
                                                                    
                                    <CardItem style={styles.button} button onPress={() => { this.onAddtoCart(item) }}>
                                        <Text style={{ color: 'white' }}>Add to cart</Text>
                                    </CardItem>
                                </Card>
                            </View>

                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}
const imageWidth = width * 0.445;
const imageHeight = (imageWidth / 933) * 800;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.fontColor
    },
    content: {
        flex: 1,
        marginLeft: 6,
        paddingBottom: 10,
        marginBottom: 10,
    },
    package: {
        borderColor: Configs.visibleColor,
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        marginRight: 10,
    },
    contentBox: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    items: {
        backgroundColor: Configs.fontColor,
        height: width / 3.5,
        marginBottom: 10,
        shadowColor: '#2E272B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        padding: 8,
        borderRadius: 4,
    },
    packageStyle: {
        flex: 1,
        justifyContent: 'space-between',
    },
    contentItem: {
        flex: 1,
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        width: imageWidth,
        borderColor: Configs.visibleColor,
        borderTopWidth: 0,
    },
    contentBoxText2: {
        color: 'red',
        padding: 5,
        fontSize: 12,
        width: imageWidth,
        borderColor: Configs.visibleColor,
        borderTopWidth: 0,
        textDecorationLine: 'line-through', textDecorationStyle: 'solid'
    },
    button: {
        height: 40,
        backgroundColor: Configs.backgroundColor,
        borderColor: '#ffff',
        borderWidth: 1,
        borderRadius: 8,
        marginHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,

    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        user_id: state.auth.user_id,
        token: state.auth.token
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onAddtoCart: (id,name,quality) => {
            dispatch(ActionsPackagePackage.onAddtoCart(id,name,quality));
        }
    }
}

export default PagesBrandDetail = connect(mapStateToProps, mapDispatchToProps)(BrandDetail)
