/**
 * @providesModule PagesHome
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,

} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Configs from 'ConfigsIndex';
class Home extends Component {
    signup() {
        Actions.signup()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.textContents}>
                    <Text style={styles.textContent}>
                        Home in here.
                        {this.props.full_name}
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Configs.backgroundColor
    },
    textContent: {
        color: '#ffffff',
        fontSize: 25,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    linearGradient: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },


})
const mapStateToProps = (state) => {
    return {
        username: state.auth.username,
        provider_login: state.auth.provider_login,
        full_name: state.auth.full_name
    };
}
export default PagesHome = connect(mapStateToProps)(Home)