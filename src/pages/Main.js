/**
 * @providesModule PagesMain
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Dimensions,
    TextInput,
    Platform
} from 'react-native';
import { Drawer, Icon, Header } from 'native-base';
import Menu from 'PagesMenu';
import Configs from 'ConfigsIndex';
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import Home from 'ComponentsHomeHome';
import { Button } from 'react-native-elements';
const { height } = Dimensions.get('window');
class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            text: '',
            selectedTab: 'home',
            token: '',
            user_id: ''
        };
        if (this.props.song)
            this.props.song.stop()
    }
    closeDrawer = () => {
        this.drawer._root.close();
    };
    openDrawer = () => {
        this.drawer._root.open();
    };
    actionSearch = () => {
        Actions.packagelist({
            api: '/api/mobile/search/all?query=' + this.state.text,
            titlePackage: this.state.text
        });
    }
    componentDidMount() {
        this.props.getAsyncStorage();
    }
    goToCart = () => {
        Actions.pagesCart();
    }
    render() {
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                type={Platform.OS == 'ios' ? "static" : "displace"}
                content={<Menu navigator={this.navigator} closeDrawer={this.closeDrawer} />}
                onClose={() => this.closeDrawer()}
                panHandlers={null}
                openDrawerOffset={0.2}
                panOpenMask={0.20}
                tweenHandler={(ratio) => ({
                    main: { opacity: (2 - ratio) / 2 }
                })}
                tapToClose

            >
                <View style={styles.container}>
                    <Header style={{ backgroundColor: Configs.backgroundColor, }}
                        iosBarStyle='light-content'
                        androidStatusBarColor={Configs.backgroundColor}  >
                        <View style={styles.formSearch}>
                            <TouchableOpacity onPress={this.openDrawer.bind(this)} style={styles.drawer}>
                                <View style={styles.imageDrawer}>
                                    <Icon name="md-menu" size={25} style={{ color: Configs.backgroundColor }} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.imgSearch}>
                                <Icon name="md-search" size={20} style={{ color: Configs.visibleColor }} />
                            </View>
                            <View style={styles.inputSearch}>
                                <TextInput
                                    underlineColorAndroid="transparent"
                                    // autoCapitalize="none"
                                    style={styles.input}
                                    placeholder="Product name...."
                                    autoFocus={this.props.focusSearch}
                                    onChangeText={(text) => this.setState({ text })}
                                    value={this.state.text}
                                    onSubmitEditing={this.actionSearch.bind(this)}
                                />
                            </View>
                        </View>
                        <TouchableOpacity onPress={this.goToCart.bind(this)} style={{marginTop:13}}>
                            <View >
                                <Icon name="cart" size={22}/>
                            </View>
                        </TouchableOpacity>

                    </Header>
                    <Home />

                </View>
            </Drawer>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Configs.visibleColor,
        justifyContent: 'center',
    },

    formSearch: {
        backgroundColor: Configs.fontColor,
        margin: 5,
        flex: 1,
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    drawer: {
        flex: 1
    },

    imageDrawer: {
        borderRightWidth: 1,
        borderColor: Configs.visibleColor,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    imgSearch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputSearch: {
        flex: 6
    }
});
const mapStateToProps = (state, ownProps) => {
    return {
        song: state.Audio.song
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAsyncStorage: () => {
            dispatch(ActionsAuthAuth.getAsyncStorage());
        },
    }
}

export default PagesMain = connect(mapStateToProps, mapDispatchToProps)(Main)