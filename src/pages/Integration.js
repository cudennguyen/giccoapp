/**
 * @providesModule PagesIntegration
 */

import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'


import Configs from 'ConfigsIndex'
import { connect } from 'react-redux'
import Header from 'ComponentsPackageHeaderItems'
import { Container, Item, Label, Input, Button, Icon, Textarea, Content } from 'native-base'
import Error from 'ComponentsHomeError'
import TypeService from 'ComponentsServicesTypeService'
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker'
import ActionsPackagePackage from 'ActionsPackagePackage'
import UtilShowMessage from 'UtilShowMessage'

class Integration extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            phone: '',
            address: '',
            title: '',
            fileName: '',
            file: '',
            type: '',
            lat_long: '',
            introduce: '',
            fileName: ''
        }
    }
    next() {

        this.props.onSaveTempt(this.state.typeIntegration, this.state.file, this.state.name, this.state.introduce, this.state.address, this.state.phone)
        Actions.AddPackage()
    }
    onChooseImage() {
        ImagePicker.showImagePicker((response) => {
            if (response.didCancel) {
                UtilShowMessage.ToastShow('User cancelled image picker', 'danger');
            }
            else if (response.error) {
                UtilShowMessage.ToastShow('ImagePicker Error: ', response.error, 'danger');
            }
            else if (response.customButton) {
                UtilShowMessage.ToastShow('User tapped custom button: ', response.customButton, 'danger');
            }
            else {
                let source = { uri: response.uri };
                this.setState({
                    file: response,
                    fileName: response.fileName
                });
            }
        });
    }
    goToMap() {
        Actions.AddMap()
    }
    render() {
        return (
            <Container>

                <Header title='Add Integration' right='Next' next={this.next.bind(this)} />

                <Content style={styles.content}>
                    <View style={styles.buttonTypeIntegration}>
                        <TypeService />

                    </View>
                    <Error value={this.props.errorimage} />
                    <Item stackedLabel style={styles.item}>
                        <Button transparent full style={styles.buttonImage} onPress={this.onChooseImage.bind(this)}>
                            <Label style={styles.label}>
                                {this.state.fileName ? this.state.fileName : 'choose file photo'}
                            </Label>

                        </Button>
                    </Item>

                    <View style={styles.viewItem}>
                        <Label style={styles.label}>
                            Name
                        </Label>
                        <Error value={this.props.errortitle} />
                    </View>
                    <Item stackedLabel style={styles.item}>
                        <Input
                            value={this.state.name}
                            onChangeText={(text) => this.setState({ name: text })} />
                    </Item>

                    <View style={styles.viewItem}>
                        <Label style={styles.label}>
                            Introduce
                        </Label>
                        <Error value={this.props.errorintroduce} />
                    </View>
                    <Textarea
                        style={styles.textarea}
                        rowSpan={4}
                        value={this.state.introduce}
                        onChangeText={(text) => this.setState({ introduce: text })}>
                    </Textarea>

                    <View style={styles.viewItem}>
                        <Label style={styles.label}>
                            Address
                        </Label>
                        <Error value={this.props.erroraddress} />

                    </View>
                    <Item stackedLabel style={styles.item}>
                        <Input
                            value={this.state.address}
                            onChangeText={(text) => this.setState({ address: text })} />
                    </Item>

                    <View style={styles.viewItem}>
                        <Label style={styles.label}>
                            Phone
                        </Label>
                        <Error value={this.props.erroraddress} />

                    </View>
                    <Item stackedLabel style={styles.item}>
                        <Input
                            value={this.state.phone}
                            onChangeText={(text) => this.setState({ phone: text })} />
                    </Item>

                    <Error value={this.props.errorlat_long} />
                    <Item stackedLabel style={styles.item}>
                        <Button transparent style={styles.buttonMap} onPress={this.goToMap.bind(this)}>
                            <Icon name='add' />
                            <Text>{this.props.marker ? (this.props.marker.latitude + " " + this.props.marker.longitude) : "Add Map"}</Text>
                        </Button>
                    </Item>

                    <Item stackedLabel style={styles.item} />
                </Content>
            </Container>
        )
    }


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    content: {
        flex: 10,
        backgroundColor: 'white'
    },
    label: {
        color: 'black',
        fontSize: 14,
        paddingLeft: 10
    },
    item: {
        flex: 1,
        flexDirection: 'column'

    },
    viewItem: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textarea: {
        borderBottomWidth: 1,
        borderBottomColor: '#E5E7E9'
    },
    buttonImage: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    buttonTypeIntegration: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#E5E7E9'
    },
    buttonMap: {
        flexDirection: 'row'
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        currentPosition: state.Audio.currentPosition,
        sliding: state.Audio.sliding,
        token: state.auth.token,
        landmarkId: state.Package.landmarkId,
        erroraddress: state.Package.erroraddress,
        errorphone: state.Package.errorphone,
        errorintroduce: state.Package.errorintroduce,
        errorlat_long: state.Package.errorlat_long,
        errorpackage: state.Package.errorpackage,
        errortitle: state.Package.errortitle,
        errorimage: state.Package.errorimage,
        marker: state.Package.locationIntegration,

    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        onSaveTempt: (typeIntegration, file, name, introduction, address, phone) => {
            dispatch(ActionsPackagePackage.saveTempt(typeIntegration, file, name, introduction, address, phone));


        },
    }
}

export default PagesIntegration = connect(mapStateToProps, mapDispatchToProps)(Integration)