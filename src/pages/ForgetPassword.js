/**
 * @providesModule PagesForgetPassword
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image
} from 'react-native'
import emailImg from '../public/images/email.png'
import { Actions } from 'react-native-router-flux'
import Error from 'ComponentsHomeError'
import InputText from 'ComponentsHomeInputText'
import styles from 'PublicStylesStyleForgetPassword'
import Configs from 'ConfigsIndex'
import UtilShowMessage from 'UtilShowMessage'
import UtilAsyncStorage from 'UtilAsyncStorage'
export default PagesForgetPassword = class ForgetPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            error_email: '',

        };
    }
    signup() {
        Actions.signup()
    }
    resetStateErrorMgs() {
        this.setState({
            error_email: '',
        })
    }
    json_function = () => {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/api/forgotpass',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        email: this.state.email,
                    })
            }
        ).then((responseJson) => {
            if (responseJson['success'] == true) {
                UtilShowMessage.ToastShow('Email reset password gửi thành công.' + '\n' + 'Bạn hãy vào mail để biết thông tin chi tiết!!!');
            }
            else {
                this.resetStateErrorMgs();
                this.setState({ error_email: responseJson['errors'][0]['msg'] });
            }
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}></View>
                <View style={styles.forgetPassword}>
                    <Text style={styles.textContent}>
                        Please enter the email address to create a new password.
                        </Text>

                    <View style={styles.inputWrapper}>
                        <Error value={this.state.error_email} />
                        <Image source={emailImg} style={styles.inlineImg} />
                        <TextInput
                            style={[styles.inputBox, { borderColor: this.state.error_email ? '#ff4d4d' : 'rgba(255, 255, 255, 0.3)' }]}
                            source={emailImg}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder='Email'
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email} />
                    </View>

                    <View style={styles.buttonSignIn}>
                        <TouchableOpacity style={styles.button}
                            onPress={this.json_function}>
                            <Text style={styles.buttonText}>
                                Send
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 3 }}>
                </View>
            </View>
        )
    }
}