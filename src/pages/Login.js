/**
 * @providesModule PagesLogin
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Dimensions from 'Dimensions';
import Logo from 'ComponentsHomeLogo';
import Form from 'ComponentsForm';
import { Actions } from 'react-native-router-flux';
import Configs from 'ConfigsIndex';
export default PagesLogin = class Login extends Component {
    signup() {
        Actions.signup()
    }
    render() {
        return (
            <View style={styles.body}>
                <View style={styles.logoImage}>
                    <Image style={{width:120,height:150}} source={Configs.logoImage} />
                </View>
                <View style={styles.container}>
                    <Form type="Login" />
                    <View style={styles.signupTextCont}>
                        <TouchableOpacity onPress={this.signup}><Text style={styles.signupbutton}>Signup</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor:"rgb(248,248,255)"
    },
    logoImage: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    container: {
        flex: 4,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    signupTextCont: {
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    signupText: {
        color: 'rgba(255, 255, 255, 0.6)',
        fontSize: 16
    },
    signupbutton: {
        color: 'black',
        fontSize: 14
    }
})

