/**
 * @providesModule PagesCart
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions
} from 'react-native'
const { width } = Dimensions.get('window');
// import emailImg from '../public/images/email.png'
// import { Actions } from 'react-native-router-flux'
// import Error from 'ComponentsHomeError'
import Header from 'ComponentsPackageHeaderItems';
import { Container,Text, Content, Card, CartItem, CardItem, Footer } from 'native-base';
// import styles from 'PublicStylesStyleForgetPassword'
import Configs from 'ConfigsIndex'
// import UtilShowMessage from 'UtilShowMessage'
import UtilAsyncStorage from 'UtilAsyncStorage'
import { Button } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
export default Cart = class Cart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            error_email: '',
            cart: [],
            total: 0,
            totalST:''
        };
    }
    componentDidMount() {
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/mobile/cart/list",
            {
                method: 'GET',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
             
            }).then((responseJson) => {
                this.setState({ cart: responseJson.items , totalST: responseJson.subTotalString, total:responseJson.subTotal })               
                
            })
    }
    changeQuantity=(quantity,id)=>{
        if(quantity>0){
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/mobile/cart/update-quantity",
        {
            method: 'POST',
            headers:
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                {
                    cartItemId: id,
                    quantity: quantity,
                })

            

        }).then((responseJson) => {            
            this.setState({ cart: responseJson.items , totalST: responseJson.subTotalString, total:responseJson.subTotal })                 

        })
    }
    }
    moveItems (id){
        UtilAsyncStorage.fetchAPI(Configs.hostname + "/api/mobile/cart/remove",
        {
            method: 'POST',
            headers:
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                {
                    itemId: id                
                })
        }).then((responseJson) => {      
            this.setState({ cart: responseJson.items , totalST: responseJson.subTotalString, total:responseJson.subTotal })               
        })
    }
    render() {
        return (
            <Container>
                <Header title={"Cart"}    cart={false}/>
                <Content>
                    {
                        this.state.cart.map((item, index) =>
                            <Card>
                                <CardItem cardBody button
                                    style={{ flexDirection: 'row' }}>
                                    <Image style={styles.imageStyle} source={{ uri: Configs.hostname + item.productImage }} />
                                    <Text numberOfLines={1} style={styles.contentBoxText} >{item.productName}</Text>
                                    <TouchableOpacity onPress={()=>this.changeQuantity(item.quantity-1,item.id)}><Text>  -  </Text></TouchableOpacity>       
                                    <Text>{item.quantity}</Text>     
                                    <TouchableOpacity onPress={()=>this.changeQuantity(item.quantity+1,item.id)}><Text>  +  </Text></TouchableOpacity>                               
                                    <Text style={{ marginLeft: 'auto', right: 10 }}>{item.productPrice}</Text>
                                    <TouchableOpacity onPress={()=>this.moveItems(item.id)}><Text>  x  </Text></TouchableOpacity>  
                                </CardItem>
                            </Card>
                        )
                    }

                </Content>
                <Footer style={{ backgroundColor: Configs.backgroundColor }}>
                    <Card>
                        <CardItem cardBody button
                            style={{ flexDirection: 'row' }}>
                            {this.state.totalST!="$0.00"?<TouchableOpacity  style={{ marginLeft: 'auto', right: 20 }} onPress={()=>Actions.Payment({count : this.state.totalST})}><Text>Thanh toán</Text></TouchableOpacity>:null}                          
                            <Text style={{ marginLeft: 'auto', right: 10 }}>Tổng tiền :</Text>
                            <Text style={{ marginLeft: 'auto', right: 10 }}>{this.state.totalST}</Text>
                        </CardItem>
                    </Card>
                </Footer>
            </Container>
        )
    }
}
const imageWidth = width * 0.2;
const imageHeight = 70;
const styles = StyleSheet.create({

    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    contentBoxText: {
        color: '#000000',
        padding: 5,
        fontSize: 12,
        width: imageWidth * 1.5,
        borderTopWidth: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    }
});


